

   MEMBER('WorTrnis.clw')                                  ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE

                     MAP
                       INCLUDE('WORTRNIS001.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Main_notused         PROCEDURE                             ! Declare Procedure

  CODE
!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Browse_Manifests PROCEDURE (STRING p:TID, BYTE p:Transfer=0)

CurrentTab           STRING(80)                            ! 
LOC:User_Access      LONG                                  ! 
DebugUserAccess      BYTE                                  ! 
LOC:Browse_Vars_Group GROUP,PRE()                          ! 
L_BV:State           STRING(15)                            ! State of this manifest
L_BV:FromAddress     STRING(35)                            ! Name of this address
L_BV:ToAddress       STRING(35)                            ! Name of this address
L_BV:NoDIs           ULONG                                 ! 
L_BV:DIs_Value       DECIMAL(10,2)                         ! 
                     END                                   ! 
LOC:Screen_Group     GROUP,PRE(L_SG)                       ! 
BID                  ULONG                                 ! Branch ID
BranchName           STRING(35)                            ! Branch Name
ManifestedState      BYTE                                  ! State of this manifest
DI_Info              BYTE                                  ! Show DI info. (No. DI's and DI's Value)
Locator              ULONG                                 ! 
Date_Locator         DATE                                  ! 
Print_Pop_Options    STRING(100)                           ! 
LimitRecords         STRING(20)                            ! Limit records displayed to this
FloorID              ULONG                                 ! Floor ID
                     END                                   ! 
LOC:1st_Time         BYTE                                  ! for Q setting
LOC:Del_Man_Sec      BYTE                                  ! 
Nulls                DATE                                  ! 
Limit_Group          GROUP,PRE(LL_G)                       ! 
NoDays               ULONG                                 ! 
Date                 LONG                                  ! 
FromDate             LONG                                  ! 
ToDate               DATE                                  ! 
                     END                                   ! 
LOC:Current_Filter_Group GROUP,PRE(L_CFG)                  ! 
Order                STRING(30)                            ! 
Filter               STRING(1001)                          ! 
                     END                                   ! 
BRW1::View:Browse    VIEW(Manifest)
                       PROJECT(MAN:MID)
                       PROJECT(MAN:Broking)
                       PROJECT(MAN:CreatedDate)
                       PROJECT(MAN:Cost)
                       PROJECT(MAN:Rate)
                       PROJECT(MAN:VATRate)
                       PROJECT(MAN:DepartDate)
                       PROJECT(MAN:DepartTime)
                       PROJECT(MAN:ETADate)
                       PROJECT(MAN:BID)
                       PROJECT(MAN:TID)
                       PROJECT(MAN:VCID)
                       PROJECT(MAN:JID)
                       PROJECT(MAN:DRID)
                       PROJECT(MAN:TTID_FreightLiner)
                       PROJECT(MAN:TTID_Trailer)
                       PROJECT(MAN:TTID_SuperLink)
                       PROJECT(MAN:State)
                       PROJECT(MAN:CollectionAID)
                       PROJECT(MAN:DeliveryAID)
                       JOIN(BRA:PKey_BID,MAN:BID)
                         PROJECT(BRA:BranchName)
                         PROJECT(BRA:BID)
                       END
                       JOIN(JOU:PKey_JID,MAN:JID)
                         PROJECT(JOU:Journey)
                         PROJECT(JOU:FID)
                         PROJECT(JOU:FID2)
                         PROJECT(JOU:JID)
                       END
                       JOIN(VCO:PKey_VCID,MAN:VCID)
                         PROJECT(VCO:CompositionName)
                         PROJECT(VCO:VCID)
                       END
                       JOIN(TRA:PKey_TID,MAN:TID)
                         PROJECT(TRA:TransporterName)
                         PROJECT(TRA:TID)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
MAN:MID                LIKE(MAN:MID)                  !List box control field - type derived from field
MAN:Broking            LIKE(MAN:Broking)              !List box control field - type derived from field
MAN:Broking_Icon       LONG                           !Entry's icon ID
MAN:Broking_Style      LONG                           !Field style
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BranchName_Style   LONG                           !Field style
MAN:CreatedDate        LIKE(MAN:CreatedDate)          !List box control field - type derived from field
JOU:Journey            LIKE(JOU:Journey)              !List box control field - type derived from field
MAN:Cost               LIKE(MAN:Cost)                 !List box control field - type derived from field
L_BV:NoDIs             LIKE(L_BV:NoDIs)               !List box control field - type derived from local data
L_BV:DIs_Value         LIKE(L_BV:DIs_Value)           !List box control field - type derived from local data
VCO:CompositionName    LIKE(VCO:CompositionName)      !List box control field - type derived from field
TRA:TransporterName    LIKE(TRA:TransporterName)      !List box control field - type derived from field
L_BV:State             LIKE(L_BV:State)               !List box control field - type derived from local data
MAN:Rate               LIKE(MAN:Rate)                 !List box control field - type derived from field
MAN:VATRate            LIKE(MAN:VATRate)              !List box control field - type derived from field
MAN:DepartDate         LIKE(MAN:DepartDate)           !List box control field - type derived from field
MAN:DepartTime         LIKE(MAN:DepartTime)           !List box control field - type derived from field
MAN:ETADate            LIKE(MAN:ETADate)              !List box control field - type derived from field
L_BV:FromAddress       LIKE(L_BV:FromAddress)         !List box control field - type derived from local data
L_BV:ToAddress         LIKE(L_BV:ToAddress)           !List box control field - type derived from local data
MAN:BID                LIKE(MAN:BID)                  !List box control field - type derived from field
MAN:TID                LIKE(MAN:TID)                  !List box control field - type derived from field
MAN:VCID               LIKE(MAN:VCID)                 !List box control field - type derived from field
MAN:JID                LIKE(MAN:JID)                  !List box control field - type derived from field
MAN:DRID               LIKE(MAN:DRID)                 !List box control field - type derived from field
MAN:TTID_FreightLiner  LIKE(MAN:TTID_FreightLiner)    !List box control field - type derived from field
MAN:TTID_Trailer       LIKE(MAN:TTID_Trailer)         !List box control field - type derived from field
MAN:TTID_SuperLink     LIKE(MAN:TTID_SuperLink)       !List box control field - type derived from field
MAN:State              LIKE(MAN:State)                !Browse hot field - type derived from field
MAN:CollectionAID      LIKE(MAN:CollectionAID)        !Browse hot field - type derived from field
MAN:DeliveryAID        LIKE(MAN:DeliveryAID)          !Browse hot field - type derived from field
JOU:FID                LIKE(JOU:FID)                  !Browse hot field - type derived from field
JOU:FID2               LIKE(JOU:FID2)                 !Browse hot field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !Related join file key field - type derived from field
JOU:JID                LIKE(JOU:JID)                  !Related join file key field - type derived from field
VCO:VCID               LIKE(VCO:VCID)                 !Related join file key field - type derived from field
TRA:TID                LIKE(TRA:TID)                  !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDB8::View:FileDrop  VIEW(Branches)
                       PROJECT(BRA:BranchName)
                       PROJECT(BRA:BID)
                     END
Queue:FileDrop       QUEUE                            !
BRA:BranchName         LIKE(BRA:BranchName)           !List box control field - type derived from field
BRA:BID                LIKE(BRA:BID)                  !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Manifest File'),AT(,,511,303),FONT('Tahoma',8,,FONT:regular),RESIZE,CENTER, |
  COLOR(00F0FFF0h),GRAY,IMM,MAX,MDI,HLP('Browse_Manifests'),SYSTEM
                       LIST,AT(8,37,493,218),USE(?Browse:1),HVSCROLL,ALRT(CtrlA),ALRT(CtrlU),FORMAT('30R(2)|FM~' & |
  'MID~C(0)@n_10@E(,0D7EBFFH,,)12R(2)|FMIY~Broking~L(1)@p p@E(,0D7EBFFH,,)40L(2)|MY~Bra' & |
  'nch~C(0)@s35@38R(2)|M~Created Date~L(1)@d5@70L(2)|M~Journey~C(0)@s70@44R(1)|M~Cost~C' & |
  '(0)@n-14.2@32R(1)|M~No. DI''s~C(0)@n13b@38R(1)|M~DI''s Value~C(0)@n-14.2b@70L(2)|M~V' & |
  'ehicle Composition~C(0)@s35@70L(2)|M~Transporter~C(0)@s35@40L(2)|M~State~C(0)@s15@38' & |
  'R(1)|M~Rate~C(0)@n-13.4@36R(1)|M~VAT Rate~C(0)@n-7.2@38R(2)|M~Depart Date~L(1)@d5b@3' & |
  '8R(2)|M~Depart Time~L(1)@t7b@38R(2)|M~ETA Date~L(1)@d5b@70L(2)|M~From Address~C(0)@s' & |
  '35@70L(2)|M~To Address~C(0)@s35@[30R(2)|M~BID~C(0)@n_10@40R(2)|M~TID~C(0)@n_10@40R(2' & |
  ')|M~VCID~C(0)@n_10@40R(2)|M~JID~C(0)@n_10@40R(2)|M~DRID~C(1)@n_10@40R(2)|M~Horse TTI' & |
  'D~C(1)@n_10@40R(2)|M~Trailer TTID~C(1)@n_10@40R(2)|M~Super Link TTID~L(1)@n_10@]|'),FROM(Queue:Browse:1), |
  IMM,MSG('Browsing the Manifest file')
                       BUTTON('&Select'),AT(237,260,49,14),USE(?Select:2),LEFT,ICON('WASELECT.ICO'),FLAT,MSG('Select the Record'), |
  TIP('Select the Record')
                       BUTTON('&View'),AT(293,260,49,14),USE(?View:3),LEFT,ICON('WAVIEW.ICO'),FLAT,MSG('View Record'), |
  TIP('View Record')
                       BUTTON('&Insert'),AT(345,260,49,14),USE(?Insert:4),LEFT,ICON('WAINSERT.ICO'),FLAT,MSG('Insert a Record'), |
  TIP('Insert a Record')
                       BUTTON('&Change'),AT(399,260,49,14),USE(?Change:4),LEFT,ICON('WACHANGE.ICO'),DEFAULT,FLAT, |
  MSG('Change the Record'),TIP('Change the Record')
                       BUTTON('&Delete'),AT(451,260,49,14),USE(?Delete:4),LEFT,ICON('WADELETE.ICO'),FLAT,MSG('Delete the Record'), |
  TIP('Delete the Record')
                       BUTTON('&Print'),AT(399,284,,14),USE(?Button_Print),LEFT,ICON(ICON:Print1),FLAT
                       CHECK(' DI Info.'),AT(451,24),USE(L_SG:DI_Info),MSG('Show DI info. (No. DI''s and DI''s Value)'), |
  TIP('Show DI info. (No. DI''s and DI''s Value)'),TRN
                       SHEET,AT(4,4,503,276),USE(?CurrentTab),JOIN
                         TAB('&1) By MID'),USE(?Tab:2)
                           PROMPT('Locator:'),AT(9,24),USE(?L_SG:Locator:Prompt),TRN
                           STRING(@n_10),AT(39,24),USE(L_SG:Locator),RIGHT(1),TRN
                           PROMPT('Limit Info'),AT(125,24,305),USE(?LimitInfo),CENTER,HIDE,TRN
                           STRING(' '),AT(101,262,107,10),USE(?String_Tagged),TRN
                           BUTTON('&Transfer'),AT(7,260,71,14),USE(?Button_Transfer),FONT(,,,FONT:bold),LEFT,ICON('1396494615' & |
  '_22999.ico'),FLAT,HIDE,TIP('Transfer the selected Manifests to your Branch')
                         END
                         TAB('&2) By Date'),USE(?Tab6)
                           PROMPT('Date Locator:'),AT(9,24),USE(?L_SG:Date_Locator:Prompt),TRN
                           ENTRY(@d5b),AT(57,23,65,10),USE(L_SG:Date_Locator),RIGHT(1)
                         END
                         TAB('&3) By Transporter'),USE(?Tab:3)
                           BUTTON('Select Transporter'),AT(9,260,,14),USE(?SelectTransporter),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&4) By Vehicle Comp.'),USE(?Tab:4)
                           BUTTON('Select Vehicle Comp.'),AT(9,260,,14),USE(?SelectVehicleComposition),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&5) By Collection'),USE(?Tab:5)
                           BUTTON('Select Address'),AT(9,260,,14),USE(?SelectAddresses),LEFT,ICON('WAPARENT.ICO'),FLAT, |
  MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                         TAB('&6) By Delivery'),USE(?Tab:6)
                           BUTTON('Select Address'),AT(9,260,,14),USE(?SelectAddressAlias),LEFT,ICON('WAPARENT.ICO'), |
  FLAT,MSG('Select Parent Field'),TIP('Select Parent Field')
                         END
                       END
                       GROUP,AT(2,284,355,10),USE(?Group_Man_State)
                         PROMPT('Manifest State:'),AT(2,286),USE(?L_SG:ManifestedState:Prompt)
                         LIST,AT(54,286,60,10),USE(L_SG:ManifestedState),VSCROLL,DROP(15),FROM('Loading|#0|Loade' & |
  'd|#1|On Route|#2|Transferred|#3|All|#255'),MSG('State of this manifest'),TIP('State of m' & |
  'anifests to show')
                         PROMPT('Arrivals for Branch:'),AT(119,286,99),USE(?Prompt_Branch),FONT(,,,FONT:regular),RIGHT
                         LIST,AT(222,286,83,10),USE(L_SG:BranchName),VSCROLL,DROP(15),FORMAT('140L(2)|M~Branch N' & |
  'ame~@s35@40R(2)|M~BID~L@n_10@'),FROM(Queue:FileDrop)
                       END
                       BUTTON('&Close'),AT(456,284,49,14),USE(?Close),LEFT,ICON('WACLOSE.ICO'),FLAT,MSG('Close Window'), |
  TIP('Close Window')
                       PROMPT('Limit Records:'),AT(349,7),USE(?L_SG:LimitRecords:Prompt),HIDE
                       ENTRY(@s20),AT(398,7,39,10),USE(L_SG:LimitRecords),HIDE,MSG('Limit records displayed<0DH>' & |
  '<0AH>eg,  <0DH,0AH>7 = last 7 days<0DH,0AH>01/01/14 = records from 1 Jan 14<0DH,0AH>' & |
  '01/01/14-15/01/14 = records from 1 Jan to 14 Jan'),TIP('Limit records displayed to this')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW_Manifests        CLASS(BrowseClass)                    ! Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
SetSort                PROCEDURE(BYTE OrderNumber),BYTE,PROC,DERIVED
UpdateWindow           PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass               ! Default Locator
BRW1::Sort0:StepClass StepRealClass                        ! Default Step Manager
BRW1::Sort1:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 3
BRW1::Sort2:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 4
BRW1::Sort3:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 5
BRW1::Sort4:StepClass StepRealClass                        ! Conditional Step Manager - CHOICE(?CurrentTab) = 6
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDB8                 CLASS(FileDropClass)                  ! File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
ResetQueue             PROCEDURE(BYTE Force=0),LONG,PROC,DERIVED
                     END

p_Client_Tags              shpTagClass


View_Man                VIEW(Manifest)
    PROJECT(MAN:MID)
                        .

Man_View                ViewManager

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?Browse:1
  !------------------------------------
!---------------------------------------------------------------------------
Un_Tag_All                      ROUTINE
       p_Client_Tags.ClearAllTags()
       BRW_Manifests.ResetFromBuffer()
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    EXIT



Tag_All                          ROUTINE
   DATA

R_Buffer    USHORT
CODE
       ! Tag All in the current sort order...
       !PUSHBIND()
       !BIND('')
   !db.Debugout('-----------loopQ-----------')

   BRW_Manifests.UpdateViewRecord()
   R_Buffer  = Access:Manifest.SaveBuffer()
      
   
   Man_View.Init(View_Man, Relate:Manifest)
   Man_View.AddSortOrder(MAN:PKey_MID)
   Man_View.SetFilter(L_CFG:Filter)

   Man_View.Reset()
   LOOP
      IF Man_View.Next() ~= LEVEL:Benign
         BREAK
      .

      p_Client_Tags.MakeTag(MAN:MID)
      !db.Debugout('-----------loopQ----------- - tagging: ' & MAN:MID)
   .

   Man_View.Kill()
!    POPBIND()
   ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()

   Access:Manifest.RestoreBuffer(R_Buffer,1)
   BRW_Manifests.ResetFromBuffer()
   
      
   

!       CASE CHOICE(?CurrentTab)
!       OF 2                        ! Delivery
!       OF 3                        ! Branch
!       OF 4                        ! Client                - Tag on
!       OF 5                        ! Client No.
!       OF 6                        ! DID
!       ELSE                        ! Invoice No.
!       .

       !Man_View.SetFilter('')

   EXIT
Tag_Toggle_this_rec       ROUTINE
    DATA
Choice_#            LONG

    CODE
       Choice_#        = CHOICE(?Browse:1)

       !LOC:Last_Tagged_Rec_Id       = CLI:CID

       IF p_Client_Tags.IsTagged(MAN:MID) = TRUE
          p_Client_Tags.ClearTag(MAN:MID)
   !       db.debugout('[Select_RateMod_Clients]  UN Tagging - CLI:CID: ' & CLI:CID)
       ELSE
          p_Client_Tags.MakeTag(MAN:MID)
   !       db.debugout('[Select_RateMod_Clients]  Tagging - CLI:CID: ' & CLI:CID)
       .

   !    BRW1.ResetFromBuffer()
   !    BRW1.Reset(1)
       GET(Queue:Browse:1, Choice_#)
       IF ~ERRORCODE()
          DO Style_Entry
          PUT(Queue:Browse:1)
       .
       ?String_Tagged{PROP:Text}   = 'Tagged: ' & p_Client_Tags.NumberTagged()
    EXIT
Style_Entry                       ROUTINE
    !IF LOC:Omitted_1 = FALSE
       IF p_Client_Tags.IsTagged(Queue:Browse:1.MAN:MID) = TRUE
          Queue:Browse:1.MAN:Broking_Style                        = 1
          Queue:Browse:1.BRA:BranchName_Style                     = 1
      ELSE
          Queue:Browse:1.MAN:Broking_Style                        = 0
          Queue:Browse:1.BRA:BranchName_Style                     = 0
    .!  .

    EXIT

!INV:DINo              
!LOC:Status           
!INV:InvoiceDate      
!INV:Weight           
!LOC:Terms            
!L_BI:Outstanding     
!INV:Total            
!L_BI:Payments        
!L_BI:Credited        
!INV:Insurance        
!INV:Documentation    
!INV:FuelSurcharge    
!INV:FreightCharge    
!INV:VAT              
!LOC:BranchName       
!INV:VolumetricWeight 
!INV:Printed          
!INV:Printed_Icon     
!INV:ShipperName      
!INV:ConsigneeName    
!INV:MIDs             
!INV:InvoiceTime      
!INV:BID              
!INV:DID              
!INV:ICID             
!INV:CID              
!INV:Terms            
!INV:Status           
Style_Setup                     ROUTINE
!    IF ~OMITTED(1)
       ! Style:Normal
   !    ?list1{PROPSTYLE:FontStyle, 1}     = FONT:Regular
   !    ?list1{PROPSTYLE:TextColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:BackColor, 1}     = COLOR:NONE
   !    ?list1{PROPSTYLE:TextSelected, 1}  = COLOR:NONE
   !    ?list1{PROPSTYLE:BackSelected, 1}  = COLOR:NONE

       ! Style:Header
       !?list1{PROPSTYLE:FontStyle, 1}     = FONT:Bold
       ?Browse:1{PROPSTYLE:TextColor, 1}     = COLOR:HIGHLIGHTTEXT
       ?Browse:1{PROPSTYLE:BackColor, 1}     = COLOR:HIGHLIGHT
       ?Browse:1{PROPSTYLE:TextSelected, 1}  = COLOR:White
       ?Browse:1{PROPSTYLE:BackSelected, 1}  = COLOR:Blue

!       SELF.Q.LOC:Day_NormalFG = -2147483634
!       SELF.Q.LOC:Day_NormalBG = -2147483635
!       SELF.Q.LOC:Day_SelectedFG = -1
!       SELF.Q.LOC:Day_SelectedBG = -1

        !?LOC:Selected{PROP:Text}    = 'Show Selected (' & RECORDS(p_Addresses) & ')'
!    ELSE
       !LOC:Omitted_1    = TRUE

       !HIDE(?LOC:Selected)
!    .
    EXIT
Tag_From_To                  ROUTINE            ! -------------------  not used yet
    DATA
R:Forward_Backward              BYTE

    CODE
!    R:Current_Selected_Rec_ID       = REC:Rec_ID
!
!!    MESSAGE('Selected Rec ID: ' & R:Current_Selected_Rec_ID & '|REC:TA_Date: ' & FORMAT(REC:TA_Date,@d5) & '|REC:TA_Time: ' & FORMAT(REC:TA_Time,@t4) & |
!!            '||LOC:Last_Tagged_Rec_Id: ' & LOC:Last_Tagged_Rec_Id)
!
!    ! If there is no Last Tagged then this is the only record to tag
!    IF LOC:Last_Clicked_Rec_ID ~= 0
!       LOC:Last_Tagged_Rec_Id   = LOC:Last_Clicked_Rec_ID
!    .
!
!    IF LOC:Last_Tagged_Rec_Id = 0
!       InBox_Tags.MakeTag(R:Current_Selected_Rec_ID)
!
!       LOC:Last_Tagged_Rec_Id       = R:Current_Selected_Rec_ID
!       LOC:Last_Tagged_Date         = REC:TA_Date
!       LOC:Last_Tagged_Time         = REC:TA_Time
!    ELSE
!       ! The beginning position of the loop depends on the last tagged record being before or after the
!       ! currently selected one.
!       R:Forward_Backward           = 0
!       IF REC:TA_Date >= LOC:Last_Tagged_Date
!          IF REC:TA_Date = LOC:Last_Tagged_Date
!             IF REC:TA_Time >= LOC:Last_Tagged_Time
!                IF REC:TA_Time = LOC:Last_Tagged_Time
!                   IF R:Current_Selected_Rec_ID < LOC:Last_Tagged_Rec_Id
!                      R:Forward_Backward  = 1
!                .  .
!             ELSE
!                R:Forward_Backward  = 1
!          .  .
!       ELSE
!          R:Forward_Backward        = 1
!       .
!
!
!       IF R:Forward_Backward = 0
!          CLEAR(A_REC:Record,-1)
!
!          ! Forward meaning from earliest Last Tagged Date / Time - file is in descending date / time order
!          ! So we set these to the later date / time here
!          A_REC:User_ID_Private_To      = LOC:User_ID
!          A_REC:TA_Date                 = REC:TA_Date
!          A_REC:TA_Time                 = REC:TA_Time
!
!          R:User_ID_Private_To          = LOC:User_ID
!          R:TA_Date                     = LOC:Last_Tagged_Date
!          R:TA_Time                     = LOC:Last_Tagged_Time
!
!          R:To_Rec_ID                   = LOC:Last_Tagged_Rec_Id
!       ELSE
!          CLEAR(A_REC:Record,+1)
!
!          A_REC:User_ID_Private_To      = LOC:User_ID
!          A_REC:TA_Date                 = LOC:Last_Tagged_Date
!          A_REC:TA_Time                 = LOC:Last_Tagged_Time
!
!          R:User_ID_Private_To          = LOC:User_ID
!          R:TA_Date                     = REC:TA_Date
!          R:TA_Time                     = REC:TA_Time
!          R:To_Rec_ID                   = REC:Rec_ID
!       .
!
!       ! The Key used does not have a field below the Time, so if the date and time in the From and To are the
!       ! same then the Records will be in record no. order or received order!
!       R:From_Rec_ID    = 0
!       IF (LOC:Last_Tagged_Date = REC:TA_Date) AND (LOC:Last_Tagged_Time = REC:TA_Time)
!          IF R:Current_Selected_Rec_ID ~= LOC:Last_Tagged_Rec_Id
!             ! Then the greater Rec_ID must be the To Rec_ID
!             IF LOC:Last_Tagged_Rec_Id > R:Current_Selected_Rec_ID
!                R:To_Rec_ID     = LOC:Last_Tagged_Rec_Id
!                R:From_Rec_ID   = R:Current_Selected_Rec_ID
!             ELSE
!                R:To_Rec_ID     = R:Current_Selected_Rec_ID
!                R:From_Rec_ID   = LOC:Last_Tagged_Rec_Id
!       .  .  .
!
!       SET(A_REC:SKEY_Adjust_Date_Time, A_REC:SKEY_Adjust_Date_Time)
!       LOOP
!          IF Access:Received_Msgs_Alias.TryNext() ~= LEVEL:Benign
!             BREAK
!          .
!
!!       IF R:Forward_Backward = 0
!!          MESSAGE('Set Date / Time: ' & FORMAT(REC:TA_Date,@d5) & ' / ' & FORMAT(REC:TA_Time, @t4) & '||' |
!!                   & 'Found Date / Time: ' & FORMAT(A_REC:TA_Date,@d5) & ' / ' & FORMAT(A_REC:TA_Time, @t4) |
!!                   & '||A_REC:Rec_ID: ' & REC:Rec_ID & ' - to Rec_ID: ' & R:To_Rec_ID,'Forward')
!!       ELSE
!!          MESSAGE('Set Date / Time: ' & FORMAT(LOC:Last_Tagged_Date,@d5) & ' / ' & FORMAT(LOC:Last_Tagged_Time, @t4) & '||' |
!!                   & 'Found Date / Time: ' & FORMAT(A_REC:TA_Date,@d5) & ' / ' & FORMAT(A_REC:TA_Time, @t4) |
!!                   & '||A_REC:Rec_ID: ' & REC:Rec_ID & ' - to Rec_ID: ' & R:To_Rec_ID,'Backwards')
!!       .
!
!          IF A_REC:User_ID_Private_To ~= LOC:User_ID      |
!                OR A_REC:TA_Date < R:TA_Date OR (A_REC:TA_Date = R:TA_Date AND A_REC:TA_Time < R:TA_Time)
!             BREAK
!          .
!
!          IF R:From_Rec_ID ~= 0
!             !MESSAGE('cycles R:From_Rec_ID < A_REC:Rec_ID||' & R:From_Rec_ID & ' < ' & A_REC:Rec_ID,'To Rec_ID: ' & R:To_Rec_ID)
!             IF R:From_Rec_ID > A_REC:Rec_ID
!                CYCLE
!          .  .
!
!          InBox_Tags.MakeTag(A_REC:Rec_ID)
!
!          IF R:To_Rec_ID = A_REC:Rec_ID
!             BREAK
!    .  .  .
!
!    BRW1.ResetFromBuffer()
    EXIT




RecordLimit_Check    ROUTINE
   HIDE(?LimitInfo)
   IF CLIP(L_SG:LimitRecords) <> ''
      CLEAR(Limit_Group)
      
      IF NUMERIC(L_SG:LimitRecords)
         LL_G:NoDays = L_SG:LimitRecords
         ?LimitInfo{PROP:Text}   = 'Records showing from ' & FORMAT(TODAY() - LL_G:NoDays + 1, @d6)
         UNHIDE(?LimitInfo)
      ELSE
         ! Look for - for from to
         str_" = L_SG:LimitRecords
         LL_G:FromDate  = DEFORMAT(Get_1st_Element_From_Delim_Str(str_", '-', 1), @d6) ! dd/mm/yy
         LL_G:ToDate    = DEFORMAT(Get_1st_Element_From_Delim_Str(str_", '-', 1), @d6) ! dd/mm/yy
         ?LimitInfo{PROP:Text}   = 'Records showing for ' & FORMAT(LL_G:FromDate,@d6) & ' to ' & FORMAT(LL_G:ToDate,@d6)
         IF LL_G:ToDate = 0
            LL_G:Date   = LL_G:FromDate
            CLEAR(LL_G:FromDate)
            ?LimitInfo{PROP:Text}   = 'Records showing for ' & FORMAT(LL_G:Date,@d6)
         .
         UNHIDE(?LimitInfo)
   .  .  
         
   EXIT   

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Manifests')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  BIND('L_SG:BID',L_SG:BID)                                ! Added by: BrowseBox(ABC)
  BIND('L_SG:ManifestedState',L_SG:ManifestedState)        ! Added by: BrowseBox(ABC)
  BIND('LL_G:NoDays',LL_G:NoDays)                          ! Added by: BrowseBox(ABC)
  BIND('LL_G:Date',LL_G:Date)                              ! Added by: BrowseBox(ABC)
  BIND('Nulls',Nulls)                                      ! Added by: BrowseBox(ABC)
  BIND('LL_G:FromDate',LL_G:FromDate)                      ! Added by: BrowseBox(ABC)
  BIND('LL_G:ToDate',LL_G:ToDate)                          ! Added by: BrowseBox(ABC)
  BIND('L_BV:NoDIs',L_BV:NoDIs)                            ! Added by: BrowseBox(ABC)
  BIND('L_BV:DIs_Value',L_BV:DIs_Value)                    ! Added by: BrowseBox(ABC)
  BIND('L_BV:State',L_BV:State)                            ! Added by: BrowseBox(ABC)
  BIND('L_BV:FromAddress',L_BV:FromAddress)                ! Added by: BrowseBox(ABC)
  BIND('L_BV:ToAddress',L_BV:ToAddress)                    ! Added by: BrowseBox(ABC)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:AddressAlias.SetOpenRelated()
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Branches.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
      IF p:Transfer = 0
         IF CLIP(p:TID) ~= ''
            L_SG:ManifestedState = 255
     
            TRA:TID              = p:TID
            IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
            .
         ELSIF SELF.Request = SelectRecord
            L_SG:ManifestedState = 255
      .  .
  BRW_Manifests.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:Manifest,SELF) ! Initialize the browse manager
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  BRW_Manifests.Q &= Queue:Browse:1
  BRW_Manifests.AddSortOrder(,)                            ! Add the sort order for  for sort order 1
  BRW_Manifests.AppendOrder('+MAN:CreatedDate,+MAN:MID')   ! Append an additional sort order
  BRW_Manifests.SetFilter('((L_SG:BID = MAN:BID OR  L_SG:BID = 0) AND (L_SG:ManifestedState = 255 OR L_SG:ManifestedState = MAN:State))') ! Apply filter expression to browse
  BRW_Manifests.AddResetField(L_SG:BID)                    ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:ManifestedState)        ! Apply the reset field
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon MAN:TID for sort order 2
  BRW_Manifests.AddSortOrder(BRW1::Sort1:StepClass,MAN:FKey_TID) ! Add the sort order for MAN:FKey_TID for sort order 2
  BRW_Manifests.AddRange(MAN:TID,Relate:Manifest,Relate:Transporter) ! Add file relationship range limit for sort order 2
  BRW_Manifests.AppendOrder('+MAN:CreatedDate,+MAN:MID')   ! Append an additional sort order
  BRW_Manifests.SetFilter('((L_SG:BID = MAN:BID OR  L_SG:BID = 0) AND (L_SG:ManifestedState = 255 OR L_SG:ManifestedState = MAN:State))') ! Apply filter expression to browse
  BRW_Manifests.AddResetField(L_SG:BID)                    ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:DI_Info)                ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:ManifestedState)        ! Apply the reset field
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon MAN:VCID for sort order 3
  BRW_Manifests.AddSortOrder(BRW1::Sort2:StepClass,MAN:FKey_VCID) ! Add the sort order for MAN:FKey_VCID for sort order 3
  BRW_Manifests.AddRange(MAN:VCID,Relate:Manifest,Relate:VehicleComposition) ! Add file relationship range limit for sort order 3
  BRW_Manifests.AppendOrder('+MAN:CreatedDate,+MAN:MID')   ! Append an additional sort order
  BRW_Manifests.SetFilter('((L_SG:BID = MAN:BID OR  L_SG:BID = 0) AND (L_SG:ManifestedState = 255 OR L_SG:ManifestedState = MAN:State))') ! Apply filter expression to browse
  BRW_Manifests.AddResetField(L_SG:BID)                    ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:DI_Info)                ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:ManifestedState)        ! Apply the reset field
  BRW1::Sort3:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon MAN:CollectionAID for sort order 4
  BRW_Manifests.AddSortOrder(BRW1::Sort3:StepClass,MAN:FKey_CAID) ! Add the sort order for MAN:FKey_CAID for sort order 4
  BRW_Manifests.AddRange(MAN:CollectionAID,Relate:Manifest,Relate:Addresses) ! Add file relationship range limit for sort order 4
  BRW_Manifests.AppendOrder('+MAN:MID')                    ! Append an additional sort order
  BRW_Manifests.SetFilter('((L_SG:BID = MAN:BID OR  L_SG:BID = 0) AND (L_SG:ManifestedState = 255 OR L_SG:ManifestedState = MAN:State))') ! Apply filter expression to browse
  BRW_Manifests.AddResetField(L_SG:BID)                    ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:DI_Info)                ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:ManifestedState)        ! Apply the reset field
  BRW1::Sort4:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon MAN:DeliveryAID for sort order 5
  BRW_Manifests.AddSortOrder(BRW1::Sort4:StepClass,MAN:FKey_DAID) ! Add the sort order for MAN:FKey_DAID for sort order 5
  BRW_Manifests.AddRange(MAN:DeliveryAID,Relate:Manifest,Relate:AddressAlias) ! Add file relationship range limit for sort order 5
  BRW_Manifests.AppendOrder('+MAN:MID')                    ! Append an additional sort order
  BRW_Manifests.SetFilter('((L_SG:BID = MAN:BID OR  L_SG:BID = 0) AND (L_SG:ManifestedState = 255 OR L_SG:ManifestedState = MAN:State))') ! Apply filter expression to browse
  BRW_Manifests.AddResetField(L_SG:BID)                    ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:DI_Info)                ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:ManifestedState)        ! Apply the reset field
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)       ! Moveable thumb based upon MAN:MID for sort order 6
  BRW_Manifests.AddSortOrder(BRW1::Sort0:StepClass,MAN:PKey_MID) ! Add the sort order for MAN:PKey_MID for sort order 6
  BRW_Manifests.AddLocator(BRW1::Sort0:Locator)            ! Browse has a locator for sort order 6
  BRW1::Sort0:Locator.Init(?L_SG:Locator,MAN:MID,1,BRW_Manifests) ! Initialize the browse locator using ?L_SG:Locator using key: MAN:PKey_MID , MAN:MID
  BRW_Manifests.SetFilter('((L_SG:BID = 0 OR L_SG:BID = MAN:BID) AND (L_SG:ManifestedState = 255 OR L_SG:ManifestedState = MAN:State) AND (LL_G:NoDays = 0 OR (MAN:CreatedDate >= (TODAY() - LL_G:NoDays + 1))) AND (LL_G:Date = Nulls OR (MAN:CreatedDate = LL_G:Date)) AND (LL_G:FromDate = Nulls OR (MAN:CreatedDate >= LL_G:FromDate AND MAN:CreatedDate <<= LL_G:ToDate)))') ! Apply filter expression to browse
  BRW_Manifests.AddResetField(L_SG:BID)                    ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:DI_Info)                ! Apply the reset field
  BRW_Manifests.AddResetField(L_SG:ManifestedState)        ! Apply the reset field
  BRW_Manifests.AddResetField(Limit_Group)                 ! Apply the reset field
  ?Browse:1{PROP:IconList,1} = '~checkoffdim.ico'
  ?Browse:1{PROP:IconList,2} = '~checkon.ico'
  BRW_Manifests.AddField(MAN:MID,BRW_Manifests.Q.MAN:MID)  ! Field MAN:MID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:Broking,BRW_Manifests.Q.MAN:Broking) ! Field MAN:Broking is a hot field or requires assignment from browse
  BRW_Manifests.AddField(BRA:BranchName,BRW_Manifests.Q.BRA:BranchName) ! Field BRA:BranchName is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:CreatedDate,BRW_Manifests.Q.MAN:CreatedDate) ! Field MAN:CreatedDate is a hot field or requires assignment from browse
  BRW_Manifests.AddField(JOU:Journey,BRW_Manifests.Q.JOU:Journey) ! Field JOU:Journey is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:Cost,BRW_Manifests.Q.MAN:Cost) ! Field MAN:Cost is a hot field or requires assignment from browse
  BRW_Manifests.AddField(L_BV:NoDIs,BRW_Manifests.Q.L_BV:NoDIs) ! Field L_BV:NoDIs is a hot field or requires assignment from browse
  BRW_Manifests.AddField(L_BV:DIs_Value,BRW_Manifests.Q.L_BV:DIs_Value) ! Field L_BV:DIs_Value is a hot field or requires assignment from browse
  BRW_Manifests.AddField(VCO:CompositionName,BRW_Manifests.Q.VCO:CompositionName) ! Field VCO:CompositionName is a hot field or requires assignment from browse
  BRW_Manifests.AddField(TRA:TransporterName,BRW_Manifests.Q.TRA:TransporterName) ! Field TRA:TransporterName is a hot field or requires assignment from browse
  BRW_Manifests.AddField(L_BV:State,BRW_Manifests.Q.L_BV:State) ! Field L_BV:State is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:Rate,BRW_Manifests.Q.MAN:Rate) ! Field MAN:Rate is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:VATRate,BRW_Manifests.Q.MAN:VATRate) ! Field MAN:VATRate is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:DepartDate,BRW_Manifests.Q.MAN:DepartDate) ! Field MAN:DepartDate is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:DepartTime,BRW_Manifests.Q.MAN:DepartTime) ! Field MAN:DepartTime is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:ETADate,BRW_Manifests.Q.MAN:ETADate) ! Field MAN:ETADate is a hot field or requires assignment from browse
  BRW_Manifests.AddField(L_BV:FromAddress,BRW_Manifests.Q.L_BV:FromAddress) ! Field L_BV:FromAddress is a hot field or requires assignment from browse
  BRW_Manifests.AddField(L_BV:ToAddress,BRW_Manifests.Q.L_BV:ToAddress) ! Field L_BV:ToAddress is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:BID,BRW_Manifests.Q.MAN:BID)  ! Field MAN:BID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:TID,BRW_Manifests.Q.MAN:TID)  ! Field MAN:TID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:VCID,BRW_Manifests.Q.MAN:VCID) ! Field MAN:VCID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:JID,BRW_Manifests.Q.MAN:JID)  ! Field MAN:JID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:DRID,BRW_Manifests.Q.MAN:DRID) ! Field MAN:DRID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:TTID_FreightLiner,BRW_Manifests.Q.MAN:TTID_FreightLiner) ! Field MAN:TTID_FreightLiner is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:TTID_Trailer,BRW_Manifests.Q.MAN:TTID_Trailer) ! Field MAN:TTID_Trailer is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:TTID_SuperLink,BRW_Manifests.Q.MAN:TTID_SuperLink) ! Field MAN:TTID_SuperLink is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:State,BRW_Manifests.Q.MAN:State) ! Field MAN:State is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:CollectionAID,BRW_Manifests.Q.MAN:CollectionAID) ! Field MAN:CollectionAID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(MAN:DeliveryAID,BRW_Manifests.Q.MAN:DeliveryAID) ! Field MAN:DeliveryAID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(JOU:FID,BRW_Manifests.Q.JOU:FID)  ! Field JOU:FID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(JOU:FID2,BRW_Manifests.Q.JOU:FID2) ! Field JOU:FID2 is a hot field or requires assignment from browse
  BRW_Manifests.AddField(BRA:BID,BRW_Manifests.Q.BRA:BID)  ! Field BRA:BID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(JOU:JID,BRW_Manifests.Q.JOU:JID)  ! Field JOU:JID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(VCO:VCID,BRW_Manifests.Q.VCO:VCID) ! Field VCO:VCID is a hot field or requires assignment from browse
  BRW_Manifests.AddField(TRA:TID,BRW_Manifests.Q.TRA:TID)  ! Field TRA:TID is a hot field or requires assignment from browse
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Browse_Manifests',QuickWindow)             ! Restore window settings from non-volatile store
      DO Style_Setup
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      IF BRA:BID = 0
         ! Load the Branch record
         BRA:BID             = GLO:BranchID
         IF Access:Branches.TryFetch(BRA:PKey_BID) = LEVEL:Benign
            L_SG:BID         = GLO:BranchID
            L_SG:BranchName  = BRA:BranchName
      .  .
  
      !Browse_Manifests PROCEDURE (STRING p:TID, BYTE p:Transfer=0)
     IF p:Transfer = 1             ! Arrivals - then set to opposite branch and On Route only - disable other tabs... see below for that
  !       IF L_SG:BID = 1
  !          L_SG:BID = 2
  !       ELSE
  !          L_SG:BID = 1
  !       .
         L_SG:ManifestedState = 2         ! On Route                             
        
         L_SG:FloorID = Get_Branch_Info(,3)    ! Current branches floor ID
        
         UNHIDE(?Button_Transfer)
  
        
        BRW_Manifests.SetFilter('(L_SG:ManifestedState = 255 OR L_SG:ManifestedState = MAN:State) AND (LL_G:NoDays = 0 OR (MAN:CreatedDate >= (TODAY() - LL_G:NoDays + 1))) AND (LL_G:Date = Nulls OR (MAN:CreatedDate = LL_G:Date)) AND (LL_G:FromDate = Nulls OR (MAN:CreatedDate >= LL_G:FromDate AND MAN:CreatedDate <= LL_G:ToDate))')
     ELSE
        ?Prompt_Branch{PROP:Text}  = 'Branch:'
        QuickWindow{PROP:Background}  = COLOR:NONE
     .
  
  
      !?Prompt_Branch{PROP:Text}   = 'Branch: ' & CLIP(L_SG:BranchName)
      IF CLIP(p:TID) ~= '' 
         DISABLE(?Tab:2)
         DISABLE(?Tab6)
         DISABLE(?Tab:4)
         DISABLE(?Tab:5)
         DISABLE(?Tab:6)
  
         DISABLE(?Insert:4)
         DISABLE(?Change:4)
         DISABLE(?Delete:4)
  
         TRA:TID    = p:TID
         IF Access:Transporter.TryFetch(TRA:PKey_TID) = LEVEL:Benign
            DISABLE(?SelectTransporter)
         .
  
      !db.debugout('[Browse_Manifest]  TRA:TransporterName: ' & CLIP(TRA:TransporterName) & ',  TRA:TID: ' & TRA:TID)
        ELSIF p:Transfer <> 0
         DISABLE(?Tab:3)
         !DISABLE(?Tab:2)
         DISABLE(?Tab6)
         DISABLE(?Tab:4)
         DISABLE(?Tab:5)
         DISABLE(?Tab:6)
  
         DISABLE(?Insert:4)
         DISABLE(?Change:4)
         DISABLE(?Delete:4)
     .  
  BRW_Manifests.AskProcedure = 1                           ! Will call: Update_Manifest
  FDB8.Init(?L_SG:BranchName,Queue:FileDrop.ViewPosition,FDB8::View:FileDrop,Queue:FileDrop,Relate:Branches,ThisWindow)
  FDB8.Q &= Queue:FileDrop
  FDB8.AddSortOrder(BRA:Key_BranchName)
  FDB8.AddField(BRA:BranchName,FDB8.Q.BRA:BranchName) !List box control field - type derived from field
  FDB8.AddField(BRA:BID,FDB8.Q.BRA:BID) !List box control field - type derived from field
  FDB8.AddUpdateField(BRA:BID,L_SG:BID)
  ThisWindow.AddItem(FDB8.WindowComponent)
  FDB8.DefaultFill = 0
      LOC:User_Access   = Get_User_Access(GLO:UID, 'Add Trans. Invoices', 'Browse_Manifests',, 1)
      DebugUserAccess   = Get_User_Access(GLO:UID, 'Browse Manifests', 'Browse_Manifests', 'Debug Menu', 1)
      
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:Default_Action)
  
      ! Returns
      !   0   - Allow (default)
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow
      !   101 - Disallow
  
      BRW_Manifests.Popup.AddItem('Manifest Email','PopupDebugSend2')
      BRW_Manifests.Popup.AddItemEvent('PopupDebugSend2',EVENT:User+2)
      BRW_Manifests.Popup.AddItem('-','Separator')
  
      IF LOC:User_Access = 0 OR LOC:User_Access = 100
         BRW_Manifests.Popup.AddItem('Create Invoices (existing need confirmation)','PopupCreateInv')
         BRW_Manifests.Popup.AddItemEvent('PopupCreateInv',EVENT:User)
         BRW_Manifests.Popup.AddItem('-','Separator')
      .
      IF DebugUserAccess = 0 OR DebugUserAccess = 100
         BRW_Manifests.Popup.AddItem('-','Separator')
         BRW_Manifests.Popup.AddItem('Send On-Route Email to fbndev','PopupDebugSend1')
         BRW_Manifests.Popup.AddItemEvent('PopupDebugSend1',EVENT:User+1)
         BRW_Manifests.Popup.AddItem('-','Separator')
      .
  
  
    
      ! =====================  Limit deleting options for Manifests  ==========================
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:Default_Action_Return)
      IF INLIST(Get_User_Access(GLO:UID, 'Manifests', 'Browse_Manifests', 'Man-Delete', 1), '0', '100') > 0
        LOC:Del_Man_Sec        = TRUE
      ELSE
        DISABLE(?Delete:4)
      .
  
  
      ! (p:UID, p:AppSection, p:Procedure, p:Extra_Section, p:SetDefault_Action, p:DefaultAction_Returned)
      ! (ULONG, STRING      , STRING     , <STRING>       , BYTE=0             , <*BYTE>                 ),LONG
      !   1       2               3               4               5                   6
      ! Returns
      !   0   - Allow (default)
      !   1   - Disable
      !   2   - Hide
      !   100 - Allow                 - Default action
      !   101 - Disallow              - Default action
      !
      !   Application Sections are added if not found
      !   Application Section Usage is logged         - not, only if not found user level????
      !
      ! p:Default_Action
      !   - applies to 1st Add only
  
      ! No Group or User actions present then default action taken
      !   Group action present then applied
      !      User action present then applied, overriding Group
      ! Check order, User specific, Group specific, default
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AddressAlias.Close
  END
  IF SELF.Opened
    INIMgr.Update('Browse_Manifests',QuickWindow)          ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

L:DIDs          STRING(255)
  CODE
      IF Request = DeleteRecord              ! (p:MID, p:DIDs)
         IF Get_Manifest_DIDs(MAN:MID, L:DIDs) > 0
      .  .
  
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    Update_Manifest
    ReturnValue = GlobalResponse
  END
      IF Request = DeleteRecord AND ReturnValue = RequestCompleted
         START(Process_Deliveries,, L:DIDs)
      .
  
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Delete:4
      ThisWindow.Update()
          ! Check that there are no Manifested items on Trip Sheets for Delivery                  - not implemented
          ! If there are then stop the delete and inform the user they must remove these items
          ! from the Trip Sheets before deleting the Manifest
    OF ?Button_Print
      ThisWindow.Update()
          BRW_Manifests.UpdateViewRecord()
          IF MAN:MID <= 0 AND p_Client_Tags.NumberTagged() <= 0
             MESSAGE('Please highlight a record in the browse.|You can also tag multiple records with CTRL & Left Click.', 'Print Manifest', ICON:Exclamation)
          ELSE
             L_SG:Print_Pop_Options = 'Print Manifest Loading|Print Manifest(s)|~Print Detailed Manifest Report'
      
             IF GLO:AccessLevel >= 10 AND p_Client_Tags.NumberTagged() <= 0
                L_SG:Print_Pop_Options = 'Print Manifest Loading|Print Manifest|Print Detailed Manifest Report|Print Manifest - Output'
             .
      
             CASE POPUP(CLIP(L_SG:Print_Pop_Options))
             OF 1
                IF p_Client_Tags.NumberTagged() > 0
                   Print_Manifests_Tagged(p_Client_Tags, 2)
      
                   CASE MESSAGE('Would you like to un-tag all tagged Manifests now?','',ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                   OF BUTTON:Yes
                      DO Un_Tag_All
                   .
                ELSE
                   Print_Manifest_Loading(MAN:MID)
                .
             OF 2
                IF p_Client_Tags.NumberTagged() > 0
                   Print_Manifests_Tagged(p_Client_Tags, 1)
      
                   CASE MESSAGE('Would you like to un-tag all tagged Manifests now?','',ICON:Question, BUTTON:Yes+BUTTON:No, BUTTON:Yes)
                   OF BUTTON:Yes
                      DO Un_Tag_All
                   .
                ELSE
                   Print_Manifest(MAN:MID)
                .
             OF 3
                Print_Manifest_Invoices(MAN:MID)
             OF 4
                Print_Manifest(MAN:MID, 1)
          .  .
      
      
    OF ?Button_Transfer
      ThisWindow.Update()
      IF p_Client_Tags.NumberTagged() <= 0
         CASE MESSAGE('Would you like to Transfer all of the Manifests shown in this list?' |
            & '||If not then click No and tag the Manifests to Transfer (use CTRL-Click or CTRL-A for all)', |
            'Transfer Manifests to My Branch', ICON:Question, BUTTON:YES+BUTTON:NO,BUTTON:YES)
         OF BUTTON:YES
            DO Tag_All
      .  .
            
      IF p_Client_Tags.NumberTagged() > 0
         Transfer_Manifests(p_Client_Tags, GLO:BranchID)
      .
      
      
      
    OF ?L_SG:Date_Locator
          CLEAR(MAN:Record,-1)
          MAN:CreatedDate = L_SG:Date_Locator
          SET(MAN:SKey_CreatedDate, MAN:SKey_CreatedDate)
          IF Access:Manifest.TryNext() = LEVEL:Benign
             BRW_Manifests.ResetFromBuffer()
          .
      
    OF ?SelectTransporter
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Transporter()
      ThisWindow.Reset
    OF ?SelectVehicleComposition
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_VehicleComposition()
      ThisWindow.Reset
    OF ?SelectAddresses
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_Address()
      ThisWindow.Reset
    OF ?SelectAddressAlias
      ThisWindow.Update()
      GlobalRequest = SelectRecord
      Select_AddressAlias()
      ThisWindow.Reset
    OF ?L_SG:LimitRecords
         IF QuickWindow{PROP:AcceptAll} = FALSE
            DO RecordLimit_Check
         .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
         CASE Keycode()
         OF CtrlA
            DO Tag_All
         OF CtrlU
            DO Un_Tag_All
         .
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
        CASE EVENT()
        OF EVENT:Accepted
           !EVENT:MouseDown
    !       BRW1.UpdateViewRecord()
    
           IF KeyCode() = ShiftMouseLeft
              ! Tag all records between this one and last tagged record
              IF CHOICE(?CurrentTab) = 1
                 DO Tag_From_To
              .
           ELSIF KeyCode() = CtrlMouseLeft
              DO Tag_Toggle_this_rec
           ELSIF KeyCode() ~= MouseRight AND KeyCode() ~= CtrlMouseRight        !AND LOC:Last_Tagged_Rec_Id ~= 0
    !          InBox_Tags.ClearAllTags()                ! Clear all tags when no shift left mouse
    !          LOC:Last_Tagged_Rec_Id        = 0
    
    !          BRW1.ResetFromBuffer()
           .
    
    !       LOC:Last_Clicked_Rec_ID      = REC:Rec_ID
    !       LOC:Last_Tagged_Date         = REC:TA_Date
    !       LOC:Last_Tagged_Time         = REC:TA_Time
        .
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?L_SG:ManifestedState
      !    ! Loading|#0|Loaded|#1|On Route|#2|Transferred|#3
      !    CASE L_SG:ManifestedState
      !    OF 'Loading'        !|#0|
      !       L_SG:ShowState   = 0
      !    OF 'Loaded'         !|#1|
      !       L_SG:ShowState   = 1
      !    OF 'On Route'       !|#2|
      !       L_SG:ShowState   = 2
      !    OF 'Transferred'    !|#3
      !       L_SG:ShowState   = 3
      !    .
          SELECT(?Browse:1)
    OF ?L_SG:BranchName
          SELECT(?Browse:1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    ELSE
      IF EVENT() = EVENT:User             ! Create Invoices
         IF RECORDS(p_Client_Tags.TagQueue) <= 0
            MESSAGE('No Manifests are tagged.','Create Invoices',ICON:Asterisk)
         ELSE
            Idx_#   = 0
            LOOP
               Idx_#    += 1
               GET(p_Client_Tags.TagQueue, Idx_#)
               IF ERRORCODE()
                  BREAK
               .
  
  
               ! (p:MID, p:Option, p:Inv_Date)
               ! p:Option
               !   0   - Ask if exists
               !   1   - Dont ask - Dont re-create
               !   2   - Dont ask - Re-create
  
               IF Gen_Transporter_Invoice(p_Client_Tags.TagQueue.Ptr, 0, TODAY()) = 0
                  !
               ELSE
      .  .  .  .
     IF EVENT() = EVENT:User + 1
        Choice_#        = CHOICE(?Browse:1)
        GET(Queue:Browse:1, Choice_#)
        IF ERRORCODE()
           MESSAGE('error getting record')
        ELSE
           Manifest_Emails_Setup(Queue:Browse:1.MAN:MID, 11, 1, 'fbndev@infosolutions.co.uk')
     .  .
     IF EVENT() = EVENT:User + 2
        Choice_#        = CHOICE(?Browse:1)
        GET(Queue:Browse:1, Choice_#)
        IF ERRORCODE()
           MESSAGE('error getting record')
        ELSE
           Manifest_Emails_Setup(Queue:Browse:1.MAN:MID)
     .  .
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW_Manifests.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1                                      ! Hide the select button when disabled
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord                              ! If called for anything other than ViewMode, make the insert, change & delete controls available
    SELF.InsertControl=?Insert:4
    SELF.ChangeControl=?Change:4
    SELF.DeleteControl=?Delete:4
  END
  SELF.ViewControl = ?View:3                               ! Setup the control used to initiate view only mode


BRW_Manifests.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSE
    RETURN SELF.SetSort(6,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW_Manifests.SetQueueRecord PROCEDURE

  CODE
      ! Loading|Loaded|On Route|Transferred
      EXECUTE MAN:State
         L_BV:State   = 'Loaded'
         L_BV:State   = 'On Route'
         L_BV:State   = 'Transferred'
      ELSE
         L_BV:State   = 'Loading'
      .
  
  
  
      ADD:AID             = MAN:CollectionAID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         L_BV:FromAddress = ADD:AddressName
      .
  
      ADD:AID     = MAN:DeliveryAID
      IF Access:Addresses.TryFetch(ADD:PKey_AID) = LEVEL:Benign
         L_BV:ToAddress   = ADD:AddressName
      .
  
  
      CLEAR(L_BV:NoDIs)
      CLEAR(L_BV:DIs_Value)
      IF L_SG:DI_Info = TRUE
         L_BV:NoDIs       = Get_Manifest_Info(MAN:MID, 5)
  
         L_BV:DIs_Value   = Get_Manifest_Info(MAN:MID, 0)     ! Vat ex.
      .
  
  PARENT.SetQueueRecord
  
  IF (MAN:Broking = 1)
    SELF.Q.MAN:Broking_Icon = 2                            ! Set icon from icon list
  ELSE
    SELF.Q.MAN:Broking_Icon = 1                            ! Set icon from icon list
  END
  SELF.Q.MAN:Broking_Style = 0 ! 
  SELF.Q.BRA:BranchName_Style = 0 ! 
      DO Style_Entry


BRW_Manifests.SetSort PROCEDURE(BYTE OrderNumber)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.SetSort(OrderNumber)
     L_CFG:Order    = self.Sort.Order
     L_CFG:Filter   = self.Sort.Filter.Filter
  
     !db.Debugout('set sort 2 range list left:  ' & self.Sort.RangeList.List.Left)
     !db.Debugout('set sort 2 range list right:  ' & self.Sort.RangeList.List.Right)
     !db.Debugout('set sort 2 range list right:  ' & self.Sort.)
     !db.Debugout('set sort 2:  ' & self.Sort.Order)
  RETURN ReturnValue


BRW_Manifests.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
  IF LOC:Del_Man_Sec ~= TRUE
     DISABLE(?Delete:4)
  .


BRW_Manifests.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
        ! Arrivals 
     IF p:Transfer = 1
        db.Debugout('****  MAN:MID: ' & MAN:MID)
        db.Debugout('****  L_SG:FloorID = JOU:FID: ' & L_SG:FloorID & ' = ' & JOU:FID &  ' = JOU:FID2: ' & JOU:FID2 & '      MAN:BID: ' & MAN:BID & '   L_SG:BID: ' & L_SG:BID)
        IF (L_SG:FloorID = JOU:FID OR L_SG:FloorID = JOU:FID2) AND MAN:BID <> L_SG:BID
           ! The current Floor is a destination of the journey, we should show it.  Otherwise not.
        ELSE
           ! Otherwise if there is no destination then show it in the origin branch   
           IF JOU:FID2 = 0 AND MAN:BID = L_SG:BID
              ! No destination FLoor, so direct, show in origin   
           ELSE
              ReturnValue = Record:Filtered
     .  .  .
  
  BRW1::RecordStatus=ReturnValue
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?Group_Man_State, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?Group_Man_State
  SELF.SetStrategy(?LimitInfo, Resize:FixXCenter+Resize:FixTop, Resize:LockSize) ! Override strategy for ?LimitInfo
  SELF.SetStrategy(?String_Tagged, Resize:FixLeft+Resize:FixBottom, Resize:LockSize) ! Override strategy for ?String_Tagged


FDB8.ResetQueue PROCEDURE(BYTE Force=0)

ReturnValue          LONG,AUTO

  CODE
  ReturnValue = PARENT.ResetQueue(Force)
      Queue:FileDrop.BRA:BranchName       = 'All'
      GET(Queue:FileDrop, Queue:FileDrop.BRA:BranchName)
      IF ERRORCODE()
         CLEAR(Queue:FileDrop)
         Queue:FileDrop.BRA:BranchName    = 'All'
         Queue:FileDrop.BRA:BID           = 0
         ADD(Queue:FileDrop)
      .
  
  
      IF LOC:1st_Time = TRUE AND CLIP(p:TID) ~= ''
         SELECT(?L_SG:BranchName, RECORDS(Queue:FileDrop))
  
         L_SG:BID         = 0
         L_SG:BranchName  = 'All'
      .
  
      LOC:1st_Time     = FALSE
  RETURN ReturnValue

!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_MainfestLoadDeliveries PROCEDURE 

CurrentTab           STRING(80)                            ! 
ActionMessage        CSTRING(40)                           ! 
LOC:Got_Group        GROUP,PRE(L_IG)                       ! 
Remaining_Units      ULONG                                 ! Remaining
Included_Units       ULONG                                 ! 
Loaded_Capacity      DECIMAL(6)                            ! In Kgs
Remaining_Capacity   DECIMAL(6)                            ! In Kgs
Total_Capacity       DECIMAL(6)                            ! In Kgs
                     END                                   ! 
LOC:Change_Group     GROUP,PRE(L_CG)                       ! 
Additional_Units     LONG                                  ! 
Additional_Weight    DECIMAL(8,2)                          ! In kg's
Total_Weight         DECIMAL(8,2)                          ! In kg's
                     END                                   ! 
LOC:DID              ULONG                                 ! Delivery ID
LOC:DIID             ULONG                                 ! Delivery Item ID
History::MALD:Record LIKE(MALD:RECORD),THREAD
QuickWindow          WINDOW('Form Mainfest Load Deliveries'),AT(,,168,233),FONT('Tahoma',8,,FONT:regular),RESIZE, |
  CENTER,GRAY,IMM,MDI,HLP('UpdateMainfestLoadDeliveries'),SYSTEM
                       SHEET,AT(4,4,161,212),USE(?CurrentTab)
                         TAB('&1) General'),USE(?Tab:1)
                           PROMPT('DIID:'),AT(101,6),USE(?MALD:DIID:Prompt),TRN
                           STRING(@n_10),AT(121,6),USE(MALD:DIID),RIGHT(1),TRN
                           PROMPT('Units Loaded:'),AT(9,22),USE(?MALD:UnitsLoaded:Prompt),TRN
                           SPIN(@n6),AT(90,22,66,10),USE(MALD:UnitsLoaded),RIGHT(1),HVSCROLL,MSG('Number of units'),TIP('Number of units')
                           GROUP('Capacities'),AT(9,96,153,55),USE(?Group2),FONT(,,COLOR:Black,FONT:regular,CHARSET:ANSI), |
  BOXED,TRN
                             PROMPT('Total:'),AT(13,108),USE(?Total_Capacity:Prompt),TRN
                             SPIN(@n-8.0),AT(90,108,66,10),USE(L_IG:Total_Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                             PROMPT('Loaded:'),AT(13,122),USE(?Loaded_Capacity:Prompt),TRN
                             SPIN(@n-8.0),AT(90,122,66,10),USE(L_IG:Loaded_Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                             PROMPT('Remaining:'),AT(13,136),USE(?L_IG:Remaining_Capacity:Prompt),TRN
                             SPIN(@n-8.0),AT(90,136,66,9),USE(L_IG:Remaining_Capacity),RIGHT(1),COLOR(00E9E9E9h),MSG('In Kgs'), |
  READONLY,SKIP,TIP('In Kgs')
                           END
                           PROMPT('Additional Units:'),AT(9,40),USE(?Additional_Units:Prompt),TRN
                           ENTRY(@n-14),AT(90,40,66,10),USE(L_CG:Additional_Units),RIGHT(1),COLOR(00E9E9E9h),READONLY, |
  SKIP
                           PROMPT('Additional Weight:'),AT(9,54),USE(?L_IG:Additional_Weight:Prompt),TRN
                           ENTRY(@n-11.2),AT(90,54,66,10),USE(L_CG:Additional_Weight),RIGHT(1),COLOR(00E9E9E9h),MSG('In kg''s'), |
  READONLY,SKIP,TIP('In kg''s')
                           PROMPT('Total Weight:'),AT(9,74),USE(?Total_Weight:Prompt),TRN
                           ENTRY(@n-11.2),AT(90,74,66,10),USE(L_CG:Total_Weight),FONT(,,COLOR:Black,FONT:bold,CHARSET:ANSI), |
  RIGHT(1),COLOR(00E9E9E9h),MSG('In kg''s'),READONLY,SKIP,TIP('In kg''s')
                           GROUP('Item Information'),AT(9,158,153,52),USE(?Group1),BOXED,TRN
                             PROMPT('Commodity:'),AT(13,168),USE(?COM:Commodity:Prompt),TRN
                             ENTRY(@s35),AT(65,168,92,10),USE(COM:Commodity),COLOR(00E9E9E9h),MSG('Commosity'),READONLY, |
  SKIP,TIP('Commosity')
                             PROMPT('Packaging:'),AT(13,182),USE(?PACK:Packaging:Prompt),TRN
                             ENTRY(@s35),AT(65,182,92,10),USE(PACK:Packaging),COLOR(00E9E9E9h),READONLY,SKIP
                             PROMPT('Units:'),AT(13,196),USE(?DELI:Units:Prompt),TRN
                             SPIN(@n6),AT(90,196,66,10),USE(DELI:Units),RIGHT(1),COLOR(00E9E9E9h),MSG('Number of units'), |
  READONLY,SKIP,TIP('Number of units')
                           END
                         END
                       END
                       BUTTON('&OK'),AT(64,218,49,14),USE(?OK),LEFT,ICON('WAOK.ICO'),DEFAULT,FLAT,MSG('Accept dat' & |
  'a and close the window'),TIP('Accept data and close the window')
                       BUTTON('&Cancel'),AT(116,218,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel operation'), |
  TIP('Cancel operation')
                       BUTTON('&Help'),AT(-2,218,49,14),USE(?Help),LEFT,ICON('WAHELP.ICO'),FLAT,HIDE,MSG('See Help Window'), |
  STD(STD:Help),TIP('See Help Window')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
New_Remaining                   ROUTINE
    L_CG:Additional_Units       = MALD:UnitsLoaded - L_IG:Included_Units
    
    IF DELI:Weight < DELI:VolumetricWeight
       L_CG:Additional_Weight   = DELI:VolumetricWeight * (L_CG:Additional_Units / DELI:Units)
    ELSE
       L_CG:Additional_Weight   = DELI:Weight           * (L_CG:Additional_Units / DELI:Units)
    .

    L_CG:Total_Weight           = L_IG:Loaded_Capacity + L_CG:Additional_Weight
    IF L_CG:Total_Weight > L_IG:Total_Capacity
       MALD:UnitsLoaded        -= 1
       L_CG:Additional_Units    = MALD:UnitsLoaded - L_IG:Included_Units
       IF L_CG:Additional_Units > 0
!          DO New_Remaining
    .  .

    DISPLAY
    EXIT

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request                                        ! Configure the action message text
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Load Item Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Load Item Record Will Be Changed'
  END
  QuickWindow{PROP:Text} = ActionMessage                   ! Display status message in title bar
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_MainfestLoadDeliveries')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MALD:DIID:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.HistoryKey = CtrlH
  SELF.AddHistoryFile(MALD:Record,History::MALD:Record)
  SELF.AddHistoryField(?MALD:DIID,3)
  SELF.AddHistoryField(?MALD:UnitsLoaded,4)
  SELF.AddUpdateFile(Access:ManifestLoadDeliveries)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
     LOC:DIID   = DELI:DIID
  Relate:DeliveryItems.SetOpenRelated()
  Relate:DeliveryItems.Open                                ! File DeliveryItems used by this procedure, so make sure it's RelationManager is open
  Access:TruckTrailer.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ManifestLoadDeliveries
  IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing ! Setup actions for ViewOnly Mode
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = Change:None
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.ChangeAction = Change:Caller                      ! Changes allowed
    SELF.CancelAction = Cancel:Cancel+Cancel:Query         ! Confirm cancel
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  SELF.Open(QuickWindow)                                   ! Open window
  Do DefineListboxStyle
  IF SELF.Request = ViewRecord                             ! Configure controls for View Only mode
    ?L_CG:Additional_Units{PROP:ReadOnly} = True
    ?L_CG:Additional_Weight{PROP:ReadOnly} = True
    ?L_CG:Total_Weight{PROP:ReadOnly} = True
    ?COM:Commodity{PROP:ReadOnly} = True
    ?PACK:Packaging{PROP:ReadOnly} = True
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
  INIMgr.Fetch('Update_MainfestLoadDeliveries',QuickWindow) ! Restore window settings from non-volatile store
  Resizer.Resize                                           ! Reset required after window size altered by INI manager
      ! Hot fields:
      !   DELI:Type
      !   DELI:ContainerNo
      !   PACK:Packaging
      !   DELI:VolumetricWeight
      !   DELI:Weight
      !   DELI:Units
      !   DEL:DINo
  
      L_IG:Included_Units         = MALD:UnitsLoaded
      L_IG:Remaining_Units        = DELI:Units - Get_ManLoadItem_Units(DELI:DIID)
  
      TRU:TTID                    = MAL:TTID
      IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = LEVEL:Benign
         L_IG:Total_Capacity      = TRU:Capacity
      .

      ! These capacities include already loaded on this record (Included_Units)
  
      L_IG:Loaded_Capacity        = Get_ManLoadItems_Info(MAL:MLID, 0)
      L_IG:Remaining_Capacity     = L_IG:Total_Capacity - L_IG:Loaded_Capacity
  

      ! Currently loaded units here + remaining in total for this Item
      ?MALD:UnitsLoaded{PROP:RangeHigh}    = L_IG:Remaining_Units + MALD:UnitsLoaded
      ?MALD:UnitsLoaded{PROP:RangeLow}     = 0

  
  
!      IF DELI:Weight < DELI:VolumetricWeight
!         L_IG:Item_Weight         = DELI:VolumetricWeight * (L_IG:Remaining_Units / DELI:Units)
!      ELSE
!         L_IG:Item_Weight         = DELI:Weight           * (L_IG:Remaining_Units / DELI:Units)
!      .
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DeliveryItems.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_MainfestLoadDeliveries',QuickWindow) ! Save window data to non-volatile store
  END
      !Upd_Delivery_Man_Status(, LOC:DIID)
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  DELI:DIID = MALD:DIID                                    ! Assign linking field value
  Access:DeliveryItems.Fetch(DELI:PKey_DIID)
  PACK:PTID = DELI:PTID                                    ! Assign linking field value
  Access:PackagingTypes.Fetch(PACK:PKey_PTID)
  COM:CMID = DELI:CMID                                     ! Assign linking field value
  Access:Commodities.Fetch(COM:PKey_CMID)
  MAL:MLID = MALD:MLID                                     ! Assign linking field value
  Access:ManifestLoad.Fetch(MAL:PKey_MLID)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord                             ! In View Only mode always signal RequestCancelled
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update()
      IF SELF.Request = ViewRecord AND NOT SELF.BatchProcessing THEN
         POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?MALD:UnitsLoaded
          DO New_Remaining
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
          DO New_Remaining
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window

!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Update_ManifestLoad_h PROCEDURE  (p:VCID)                  ! Declare Procedure
L:Idx                LONG                                  ! 
L:TTID               ULONG                                 ! 
L:MID                ULONG                                 ! Manifest ID
L:Capacity           DECIMAL(6)                            ! In Kgs
Tek_Failed_File     STRING(100)


  CODE

  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:Manifest.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoad.Open()
     .
  ! Note: If there is no Tek_Failed_File code above here then Debug is off and the files will be openned!
     IF CLIP(Tek_Failed_File) = ''                     ! We are only going to open files if we failed last time
        Relate:ManifestLoadAlias.Open()
     .
     Access:Manifest.UseFile()
     Access:ManifestLoad.UseFile()
     Access:ManifestLoadAlias.UseFile()
    ! (p:VCID)
    ! Load the Manifest

   start_#  = CLOCK()
   db.debugout('[Update_ManifestLoad_h]  In - VCID: ' & p:VCID & '      GlobalRequest = InsertRecord: ' & GlobalRequest & ' = ' & InsertRecord)
   db.debugout('[Update_ManifestLoad_h]  MAN:MID: ' & MAN:MID)

    IF GlobalRequest = InsertRecord
       L:MID        = MAN:MID
       IF Access:Manifest.TryFetch(MAN:PKey_MID) = LEVEL:Benign
          LOOP 4 TIMES
             L:Idx             += 1
             L:TTID             = Get_VehComp_Info(p:VCID, 6, L:Idx - 1, 5)
             L:Capacity         = Get_VehComp_Info(p:VCID, 5, L:Idx - 1)

             ! Check that this Truck / Trailer is not being used already on this MID
             A_MAL:MID          = L:MID
             A_MAL:TTID         = L:TTID
             IF Access:ManifestLoadAlias.TryFetch(A_MAL:SKey_MID_TTID) = LEVEL:Benign
                CLEAR(L:TTID)
                CYCLE
             .
             IF L:Capacity <= 0.0
                CLEAR(L:TTID)
                CYCLE
             .

             !db.debugout('capacity on: ' & L:TTID & ' which is no: ' & L:Idx - 1 & '  at: ' & L:Capacity)

             ! Break on 1st non loaded vehicle
             BREAK
          .
          
          db.debugout('[Update_ManifestLoad_h] - Finish Vehicle lookups - Time taken: ' & CLOCK() - start_#)
            
          IF L:TTID = 0
             ! Delete Browse newly created record
             IF MAL:TTID <= 0
                db.debugout('[Update_ManifestLoad_h]  Relate Delete - Top - MLID: ' & MAL:MLID)
                Add_Log('Removing new ManifestLoad record - MAL:MLID: ' & MAL:MLID & '   MAL:MID: ' & MAL:MID,'Update_ManifestLoad_h')
                IF Relate:ManifestLoad.Delete(0) ~= LEVEL:Benign
                   MESSAGE('Could not remove new ManifestLoad record!||Please make a note of this error and report it.', 'Manifest Load', ICON:Hand)
                   Add_Log('Could not remove new ManifestLoad record! - MAL:MLID: ' & MAL:MLID & '   MAL:MID: ' & MAL:MID,'Update_ManifestLoad_h')
                .
             ELSE
                MESSAGE('This Manifest Load already has a Truck / Trailer assigned?||TTID: ' & MAL:TTID, 'Manifest Load', ICON:Hand)
             .

             ! Call our procedure to check on existing ManifestLoad capacity
             MAL:MLID   = Ask_ManifestLoad(L:MID)
             IF MAL:MLID > 0
                IF Access:ManifestLoad.TryFetch(MAL:PKey_MLID) = LEVEL:Benign
                   GlobalRequest   = ChangeRecord
                   IF Update_ManifestLoad() < 0
                      MESSAGE('Change Record failed to Load 2.', 'Manifest Load (h)', ICON:Hand)
                   .
                ELSE
                   MESSAGE('Unable to load the Manifest Load record.||MLID: ' & MAL:MLID, 'Manifest Load (h)', ICON:Exclamation)
                   GlobalResponse   = LEVEL:Notify
                .
             ELSE
                MESSAGE('There are no more Trucks / Trailers available to load items onto for this Manifest.', 'Trucks / Trailers', ICON:Exclamation)
                GlobalResponse      = LEVEL:Notify
             .
          ELSE
             GlobalRequest          = InsertRecord
               
             IF Update_ManifestLoad() < 0
                MESSAGE('Vehicle found but rejected update?', 'Manifest Load (h)', ICON:Hand)
          .  .
       ELSE
          db.debugout('[Update_ManifestLoad_h]  Relate Delete - N Bottom - MLID: ' & MAL:MLID)
          Add_Log('Relate delete - N Bottom - MAL:MLID: ' & MAL:MLID & '   MAL:MID: ' & MAL:MID,'Update_ManifestLoad_h')

          IF Relate:ManifestLoad.Delete(0) ~= LEVEL:Benign
          .
          MESSAGE('No Manifest.', 'Manifest Load (h)', ICON:Hand)
          !Update_ManifestLoad()
       .
    ELSE
       IF Update_ManifestLoad() < 0
          MESSAGE('Change Record failed to Load.', 'Manifest Load (h)', ICON:Hand)
    .  .


    IF MAL:MLID > 0
       IF Get_ManLoadItems_Info(MAL:MLID, 1) <= 0
          IF Access:ManifestLoad.TryFetch(MAL:PKey_MLID) = LEVEL:Benign
             db.debugout('[Update_ManifestLoad_h]  Relate Delete - Bottom - MLID: ' & MAL:MLID)
             Add_Log('Relate delete - Bottom - MAL:MLID: ' & MAL:MLID & '   MAL:MID: ' & MAL:MID,'Update_ManifestLoad_h')
             IF Relate:ManifestLoad.Delete(0) ~= LEVEL:Benign
    .  .  .  .

    DO Procedure_Return
Procedure_Return    Routine
    DO CloseFiles

       Return

    Exit

CloseFiles     Routine
    Relate:Manifest.Close()
    Relate:ManifestLoad.Close()
    Relate:ManifestLoadAlias.Close()
    Exit
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Transfer_Manifests   PROCEDURE  (shpTagClass p_Client_Tags,BYTE p_To_Floor) ! Declare Procedure
LOC:MID              ULONG                                 ! 
LOC:To_Floor         ULONG                                 ! 
LOC:Continue         BYTE                                  ! 
LOC:Notifications_ON BYTE                                  ! 

  CODE
   LOC:To_Floor   = Ask_MoveFloor(0, p_To_Floor)
   LOC:Continue   = TRUE

   IF LOC:To_Floor <= 0
      ! No to floor specified.
      CASE MESSAGE('You have not specified a Floor to move the Deliveries to.||Would you still like to set status on this Manifest to Transferred?','To Floor', ICON:Question, BUTTON:Yes+BUTTON:No,BUTTON:No)
      OF BUTTON:No
         ! Do nothing
         LOC:Continue   = FALSE
   .  .

   IF LOC:Continue = TRUE
      p_Client_Tags.SetPosition()
      
      LOOP
         LOC:MID  = CLIP(p_Client_Tags.NextTagged()) 
         IF LOC:MID = 0
            BREAK
         .
         
         
         
         ! Get Manifest and progress it
         A_MAN:MID  = LOC:MID
         IF Access:ManifestAlias.TryFetch(A_MAN:PKey_MID) = Level:Benign         ! Process it
            A_MAN:State += 1                                                     ! ?????  or Just set to Transferred??
            ! ????? dont need next if perhaps
            IF A_MAN:State = 3                                                     ! Transferred
               IF Access:ManifestAlias.Update() = Level:Benign                   ! Great, then send emails????
                  IF Move_ManDeliveries(LOC:MID, LOC:To_Floor) < 0
                     MESSAGE('Not all applicable Deliveries were updated to the new Floor.', 'Floor Updates', ICON:Hand)
               .  .
                  
!               db.Debugout('----- MID: ' & LOC:MID)
                  
               LOC:Notifications_ON    = GETINI('Delivery', 'NotificationsOn', '1', GLO:Global_INI)
               IF LOC:Notifications_ON = TRUE                                    ! 27 Sep 13 - send emails for DIs that have email addresses on them......
                  Manifest_Emails_Setup(LOC:MID,12)                              ! send emails now - replies on DIs being moved to a new Floor to get branch            
   .  .  .  .  .

         
!!! <summary>
!!! Generated from procedure template - Window
!!! (p:ID, p:Option, p:Debug, p:DebugTo) - send emails silently or with wizard SETUP NOT USED HERE - SEE ManTranIS app
!!! </summary>
Manifest_Emails_Setup PROCEDURE (ULONG p:ID,BYTE p:Option,BYTE p:Debug,<STRING p:DebugTo>)

LOC_Q                QUEUE,PRE(LQ)                         ! 
Mark                 BYTE                                  ! 
ManifestInfo         STRING(200)                           ! 
TreeState            LONG                                  ! 
ManifestInfo_Style   LONG                                  ! 
EmailName            STRING(100)                           ! 
EmailAddress         STRING(255)                           ! 
EAID                 ULONG                                 ! 
DID                  ULONG                                 ! 
CID                  ULONG                                 ! 
                     END                                   ! 
DisplayString        STRING(255)                           ! 
LOC_Add_Q            QUEUE,PRE(L_AQ)                       ! 
EmailAddress         STRING(255)                           ! 
EmailName            STRING(35)                            ! 
DID                  ULONG                                 ! 
                     END                                   ! 
LOC_MLID             ULONG                                 ! 
CurrentTab           LONG                                  ! 
LOC_Mark_Q           QUEUE,PRE(L_MQ)                       ! 
DID                  ULONG                                 ! 
EmailAddress         STRING(255)                           ! Email Address
Mark                 BYTE                                  ! 
                     END                                   ! 
LOC_Email_Group      GROUP,PRE(LO_EG)                      ! 
Email_Address        STRING(255)                           ! 
Content              STRING(5000)                          ! 
Substitution         STRING(30)                            ! 
Email_Type           STRING('''n Route''kEmail Type: {14}') ! 
Prev_Email_Type      STRING(35)                            ! 
                     END                                   ! 
LOC_Loaded_Email_Group GROUP,PRE(L_EG)                     ! 
Subject              STRING(255)                           ! 
Content              STRING(2000)                          ! 
                     END                                   ! 
LOC_EmailSend_Group  GROUP,PRE(L_ESG)                      ! 
From                 STRING(255)                           ! 
Subject              STRING(255)                           ! 
CC                   STRING(255)                           ! 
BCC                  STRING(255)                           ! 
Server               STRING(255)                           ! 
Message              STRING(5000)                          ! 
HTML                 STRING(5000)                          ! 
SMTP_Username        STRING(255)                           ! 
SMTP_Password        STRING(255)                           ! 
                     END                                   ! 
Local_RecordBuffers  GROUP,PRE(LO_FB)                      ! 
TripSheet            USHORT                                ! 
Deliveries           USHORT                                ! 
                     END                                   ! 
LOC:Silent_Group     GROUP,PRE(L_SG)                       ! 
Notice_Type          BYTE                                  ! 1 - On Route, 2 - Transferred, 3 - On Delivery, 4 - Delivered, 5 - Manifest General
Load_Type            BYTE                                  ! 1 - Overnight, 2 - Broking, 3 - Local
Destination_BranchName STRING(35)                          ! Branch Name
                     END                                   ! 
QuickWindow          WINDOW('Manifest Emails'),AT(,,523,349),FONT('MS Sans Serif',8,,FONT:regular,CHARSET:DEFAULT), |
  DOUBLE,CENTER,GRAY,IMM,MDI,HLP('Manifest_Emails'),SYSTEM
                       BUTTON('&Cancel'),AT(469,331,49,14),USE(?Cancel),LEFT,ICON('WACANCEL.ICO'),FLAT,MSG('Cancel Operation'), |
  TIP('Cancel Operation')
                       STRING('String2'),AT(5,6,377,14),USE(?STRING2)
                       SHEET,AT(5,24,513,303),USE(?SHEET1),WIZARD
                         TAB('Manifest'),USE(?TAB_Manifest)
                           LIST,AT(11,43,500,214),USE(?LIST_ManifestEmails),FONT('Microsoft Sans Serif',,,,CHARSET:DEFAULT), |
  VSCROLL,ALRT(F2Key),FORMAT('18C|FM~Mark~L(2)@n-3b@400L(2)|MYT(R)~Manifest Info~@s200@'), |
  FROM(LOC_Q)
                           ENTRY(@s255),AT(72,304,247,10),USE(LO_EG:Email_Address)
                           BUTTON('Add Email'),AT(323,302),USE(?BUTTON_AddEmail),FONT('Microsoft Sans Serif',,,FONT:regular)
                           PROMPT('Email Address:'),AT(17,304),USE(?LO_EG:Email_Address:Prompt),TRN
                           PANEL,AT(11,278,501,44),USE(?PANEL1),BEVEL(-2)
                           PROMPT('Enter any additional email addresses you want to send this email to and click t' & |
  'he Add Email button.'),AT(17,284,366,14),USE(?PROMPT1),TRN
                           BUTTON('Client EMail Addresses'),AT(421,260),USE(?BUTTON_ClientEmails)
                           BUTTON('Exclude / Include'),AT(11,260),USE(?BUTTON_Exclude),TIP('Exclude / Include togg' & |
  'le the highlighted address from being emailed<0DH,0AH,0DH,0AH>F2 key shortcut')
                         END
                         TAB('Content'),USE(?TAB_Content)
                           PROMPT('The content of the email (you can edit this):'),AT(13,97),USE(?LO_EG:Content:Prompt)
                           STRING('There are X unique email addresses to which the message will be sent.'),AT(13,46,377, |
  14),USE(?STRING_Content)
                           BUTTON('Load Saved Content'),AT(13,308),USE(?BUTTON_LoadContent)
                           BUTTON('Save Content'),AT(99,308,81,14),USE(?BUTTON_SaveContent)
                           PROMPT('Subject:'),AT(13,65),USE(?L_ESG:Subject:Prompt)
                           ENTRY(@s255),AT(13,78,497,10),USE(L_ESG:Subject)
                           TEXT,AT(14,111,497,192),USE(LO_EG:Content),VSCROLL
                           LIST,AT(381,310,93,10),USE(LO_EG:Substitution,,?LO_EG:Substitution:3),DROP(10),FROM('Client Nam' & |
  'e|Collection Add|Delivery Add|Client Ref|Commodity|Items|Weight|Special Remarks|ETA|' & |
  'Branch|Dispatch Vehicle|Delivery Date/Time|Branch Tel.'),TIP('Select fields to use i' & |
  'n the content.  The actual values will be merged.')
                           PROMPT('Substitution:'),AT(338,310,,10),USE(?LO_EG:Substitution:Prompt:3)
                           BUTTON('&Add'),AT(479,308),USE(?BUTTON_Add),FONT('Microsoft Sans Serif',,,FONT:regular)
                           LIST,AT(61,31,93,10),USE(LO_EG:Email_Type,,?LO_EG:Email_Type:2),DROP(5),FROM('On Route|#' & |
  'On Route|Transferred|#Transferred|Out On Delivery|#Out On Delivery|Delivered|#Delive' & |
  'red|Manifest General|#Manifest General')
                           PROMPT('Email Type:'),AT(13,31),USE(?LO_EG:Email_Type:Prompt)
                           BUTTON('Copy to Clip.'),AT(184,308,81,14),USE(?BUTTON_CopyClip),TIP('Copy content to cl' & |
  'ipboard (can also use CTRL-C, CTRL-V')
                         END
                         TAB('Summary'),USE(?TAB_Summary)
                           PROMPT('Email ready to send.  It will be sent to X addresses.'),AT(18,51,373,18),USE(?PROMPT_Summary), |
  FONT('Microsoft Sans Serif',,,FONT:regular)
                           BUTTON('&Send'),AT(438,288,69,25),USE(?Ok:2),LEFT,ICON('WAOK.ICO'),FLAT,MSG('Accept operation'), |
  TIP('Accept Operation'),TRN
                           LIST,AT(19,79,191,234),USE(?LIST_Email_Addresses),HVSCROLL,FORMAT('1020L(2)|M~Email Add' & |
  'ress~@s255@'),FROM(LOC_Add_Q)
                           ENTRY(@s255),AT(307,128,183,10),USE(L_ESG:From)
                           PROMPT('From:'),AT(245,127),USE(?L_ESG:From:Prompt)
                           ENTRY(@s255),AT(307,152,183,10),USE(L_ESG:CC)
                           PROMPT('CC:'),AT(245,151),USE(?L_ESG:CC:Prompt)
                           ENTRY(@s255),AT(307,170,183,10),USE(L_ESG:BCC)
                           PROMPT('BCC:'),AT(245,169),USE(?L_ESG:BCC:Prompt)
                           ENTRY(@s255),AT(307,201,183,10),USE(L_ESG:Server)
                           PROMPT('Server:'),AT(245,200),USE(?L_ESG:Server:Prompt)
                           ENTRY(@s255),AT(307,222,183,10),USE(L_ESG:SMTP_Username)
                           PROMPT('SMTP Username:'),AT(245,222),USE(?L_ESG:SMTP_Username:Prompt)
                           ENTRY(@s255),AT(307,239,183,10),USE(L_ESG:SMTP_Password),PASSWORD
                           PROMPT('SMTP Password:'),AT(245,239),USE(?L_ESG:SMTP_Password:Prompt)
                           PANEL,AT(227,78,280,188),USE(?PANEL2),BEVEL(1)
                           PROMPT('Email Server and other details'),AT(245,89,245,26),USE(?PROMPT2),FONT('Microsoft ' & |
  'Sans Serif',,,FONT:bold)
                         END
                       END
                       BUTTON('&Next'),AT(414,331),USE(?BUTTON_Next)
                       BUTTON('&Back'),AT(378,331,32,14),USE(?BUTTON_Back)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Local Data Classes
ThisNetEmail         CLASS(NetEmailSend)                   ! Generated by NetTalk Extension (Class Definition)

                     END

View_               VIEW(ManifestLoad)
                      PROJECT(MAL:MLID, MAL:TTID)
                      JOIN(TRU:PKey_TTID, MAL:TTID)
                        PROJECT(TRU:Registration,TRU:Capacity,TRU:TID)
                      .
                      
                      JOIN(MAN:PKey_MID, MAL:MID)
                        
                      .

                      JOIN(MALD:FSKey_MLID_DIID, MAL:MLID)
                        PROJECT(MALD:DIID, MALD:UnitsLoaded)
                        JOIN(DELI:PKey_DIID, MALD:DIID)
                          PROJECT(DELI:DID, DELI:CMID)
                          JOIN(COM:PKey_CMID, DELI:CMID)
                            PROJECT(COM:Commodity)
                          .  
                                
                          JOIN(DEL:PKey_DID, DELI:DID)
                            PROJECT(DEL:CID, DEL:ClientReference, DEL:NoticeEmailAddresses)
                            JOIN(CLI:PKey_CID, DEL:CID)
                              PROJECT(CLI:ClientName,CLI:ClientNo)
                            .
                            
                            JOIN(EMAI:FKey_CID, DEL:CID)
                              PROJECT(EMAI:EAID,EMAI:EmailName,EMAI:EmailAddress,EMAI:DefaultAddress, |
                                EMAI:RateLetter,EMAI:Operations,EMAI:OperationsReference)
                    . . . . .
                            
View2_              VIEW(TripSheetDeliveries)
                      PROJECT(TRDI:TDID, TRDI:TRID, TRDI:UnitsLoaded, TRDI:DIID)

                      JOIN(DELI:PKey_DIID, TRDI:DIID)                        
                          PROJECT(DELI:DID, DELI:CMID)
                                
                          JOIN(DEL:PKey_DID, DELI:DID)
                            PROJECT(DEL:CID, DEL:ClientReference, DEL:NoticeEmailAddresses)
!                            JOIN(CLI:PKey_CID, DEL:CID)
!                              PROJECT(CLI:ClientName,CLI:ClientNo)
!                            .
!                            
!                            JOIN(EMAI:FKey_CID, DEL:CID)
!                              PROJECT(EMAI:EAID,EMAI:EmailName,EMAI:EmailAddress,EMAI:DefaultAddress, |
!                                    EMAI:RateLetter,EMAI:Operations,EMAI:OperationsReference)
                    . .   . !.
                              
VM_   ViewManager
                    MAP
Load_Content_File     PROCEDURE(STRING),STRING
Save_Content_File     PROCEDURE(STRING)

Load_Content         PROCEDURE(LONG, LONG),BYTE

Check_Add_Q           PROCEDURE(ULONG pDID, STRING pEmailName, STRING pEmailAddress, BYTE pOption=0)

Merge_Content         PROCEDURE(ULONG p:DID, STRING p:Content),STRING

Load_Q_DIs              PROCEDURE()

Send_Email_l              PROCEDURE(BYTE p_Silent=0)

                    .
Load_Q_Class        CLASS

VM_DD                 &ViewManager

InitMan               PROCEDURE()  
InitTrip              PROCEDURE()  

Loop_                 PROCEDURE()

Construct             PROCEDURE()
Destruct              PROCEDURE()
                    .
R_Result_Q           Q_Class
R_Error_Q           Q_Class


  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!  ************         SETUP NOT USED HERE - SEE ManTranIS app
Load_Q                      ROUTINE
  SETCURSOR(CURSOR:Wait)
  
  CASE p:Option
  OF 1
    DO Load_Q_General  
  OF 0
    DO Load_Q_DIs_Routine
  .
  
  
  SETCURSOR()
Load_Q_DIs_Routine        ROUTINE       ! not the procedure
  ! Might want to save any marked / unmarked records before a reload
  DO Save_Mark_Q
  
  FREE(LOC_Q)
  FREE(LOC_Add_Q)  
  CLEAR(LOC_MLID)
  
  !CLEAR(MAN:Record)        - assume loaded?  check ID
  IF MAN:MID ~= p:ID
    CLEAR(MAN:Record)
    MAN:MID = p:ID
    IF Access:Manifest.TryFetch(MAN:PKey_MID) ~= Level:Benign
      ! return??
    .
  .

  VM_.Init(View_, Relate:ManifestLoad)
  VM_.AddSortOrder(MAL:FKey_MID)
  VM_.AppendOrder('MAL:MLID,CLI:ClientName,DEL:DINo')
  !VM_.SetFilter('EMAI:Operations = 1')
  VM_.AddRange(MAL:MID,p:ID)
  VM_.Reset(1)

  IF ERRORCODE()
    MESSAGE('Error on view man reset: ' & ERROR())
  .

  LOOP
    IF VM_.Next() ~= Level:Benign
      BREAK
    .
    IF LOC_MLID = 0 OR LOC_MLID ~= MAL:MLID
      LOC_MLID = MAL:MLID
      
      ! Level 1 - The Truck 
      CLEAR(LOC_Q)
      LQ:ManifestInfo  = '' & CLIP(TRU:Registration) & '  (Cap: ' & CLIP(LEFT(FORMAT(TRU:Capacity, @n14.2))) & ')'
      LQ:TreeState     = 1
      ADD(LOC_Q)
    .
    
    IF CLIP(DEL:NoticeEmailAddresses) <> ''
      Check_Add_Q(DEL:DID, '', DEL:NoticeEmailAddresses)      ! Add unique address to address q
    .
    
    LQ:DID            = DELI:DID    
    GET(LOC_Q, +LQ:DID)
    IF ~ERRORCODE()     ! Dont add DI more than once... could be confusing as doesnt look like Manifest then..
      ! Dont add email address more than once either
      
      GET(LOC_Q, +LQ:DID, +LQ:EmailAddress)
      IF ERRORCODE()
        IF CLIP(DEL:NoticeEmailAddresses) <> ''
           !MESSAGE('adding 2nd add: ' & EMAI:EmailAddress )
           LQ:EmailName      = ''
           LQ:EmailAddress   = DEL:NoticeEmailAddresses
           !LQ:EAID           = EMAI:EAID
           LQ:DID            = DELI:DID    
           LQ:CID            = DEL:CID    
           LQ:TreeState      = 3

           LQ:ManifestInfo       = DEL:NoticeEmailAddresses
           LQ:Mark               = 1
           LQ:ManifestInfo_Style = 2

           ADD(LOC_Q, POINTER(LOC_Q) + 1)      ! put it in below entry found
      . . 
      CYCLE    
    .    
    
    
    CLEAR(LOC_Q)
    ! Level 2 - The Cargo           -  Get_Client_Emails(DEL:CID,2,1)
    LQ:ManifestInfo  = '[no Delivery Email address]'            ! By default
    IF CLIP(DEL:NoticeEmailAddresses) <> ''
      LQ:ManifestInfo  = 'Delivery addresses'   ! Get_Client_Emails(DEL:CID,2,1)     ! So we do have one, then replace default
    .    

    ! Then full in the general stuff
    LQ:ManifestInfo  = 'DI: ' & DEL:DINo & ' - ' & CLIP(LQ:ManifestInfo) & | 
                      '   for ' & CLIP(CLI:ClientName) & ' ' & CLIP(CLI:ClientNo) & ', ' & |
                      ' (' & MALD:UnitsLoaded & ' X ' & CLIP(COM:Commodity) & ')'
      
    LQ:TreeState      = 2
    LQ:EmailName      = '' 
    LQ:EmailAddress   = DEL:NoticeEmailAddresses
    !LQ:EAID           = EMAI:EAID
    LQ:DID            = DELI:DID    
    LQ:CID            = DEL:CID    
    LQ:Mark           = 0
    
    LQ:ManifestInfo_Style = 0
    ADD(LOC_Q)
    
    IF CLIP(DEL:NoticeEmailAddresses) <> ''
      LQ:TreeState          = 3
      LQ:Mark               = 1
      LQ:ManifestInfo_Style = 2
      LQ:ManifestInfo       = DEL:NoticeEmailAddresses

      ADD(LOC_Q)
  . .

  DO Set_Marks_From_Q
  
  EXIT
Load_Q_General          ROUTINE
  ! Might want to save any marked / unmarked records before a reload
  DO Save_Mark_Q
  
  FREE(LOC_Q)
  FREE(LOC_Add_Q)  
  CLEAR(LOC_MLID)
  
  !CLEAR(MAN:Record)        - assume loaded?  check ID
  IF MAN:MID ~= p:ID
    CLEAR(MAN:Record)
    MAN:MID = p:ID
    IF Access:Manifest.TryFetch(MAN:PKey_MID) ~= Level:Benign
      ! return??
    .
  .

  VM_.Init(View_, Relate:ManifestLoad)
  VM_.AddSortOrder(MAL:FKey_MID)
  VM_.AppendOrder('MAL:MLID,CLI:ClientName,DEL:DINo')
  !VM_.SetFilter('EMAI:Operations = 1')
  VM_.AddRange(MAL:MID,p:ID)
  VM_.Reset(1)

  IF ERRORCODE()
    MESSAGE('Error on view man reset: ' & ERROR())
  .

  LOOP
    IF VM_.Next() ~= Level:Benign
      BREAK
    .
    IF LOC_MLID = 0 OR LOC_MLID ~= MAL:MLID
      LOC_MLID = MAL:MLID
      
      ! Level 1 - The Truck 
      CLEAR(LOC_Q)
      LQ:ManifestInfo  = '' & CLIP(TRU:Registration) & '  (Cap: ' & CLIP(LEFT(FORMAT(TRU:Capacity, @n14.2))) & ')'
      LQ:TreeState     = 1
      ADD(LOC_Q)
    .
    
    IF EMAI:Operations = TRUE 
      Check_Add_Q(DEL:DID, EMAI:EmailName, EMAI:EmailAddress)      ! Add unique address to address q
    .
    
    LQ:DID            = DELI:DID    
    GET(LOC_Q, +LQ:DID)
    IF ~ERRORCODE()     ! Dont add DI more than once... could be confusing as doesnt look like Manifest then..
      ! Dont add email address more than once either
      
      GET(LOC_Q, +LQ:DID, +LQ:EmailAddress)
      IF ERRORCODE()
         IF EMAI:Operations = TRUE 
           !MESSAGE('adding 2nd add: ' & EMAI:EmailAddress )
           LQ:EmailName      = EMAI:EmailName 
           LQ:EmailAddress   = EMAI:EmailAddress
           LQ:EAID           = EMAI:EAID
           LQ:DID            = DELI:DID    
           LQ:CID            = DEL:CID    
           LQ:TreeState      = 3

           LQ:ManifestInfo       = EMAI:EmailAddress
           LQ:Mark               = 1
           LQ:ManifestInfo_Style = 2

           DO Check_Ops_Ref
             
           ADD(LOC_Q, POINTER(LOC_Q) + 1)      ! put it in below entry found
      .  .
      CYCLE    
    .    
    
    
    CLEAR(LOC_Q)
    ! Level 2 - The Cargo           -  Get_Client_Emails(DEL:CID,2,1)
    LQ:ManifestInfo  = '[no operations address]'            ! By default
    IF EMAI:Operations = TRUE
      LQ:ManifestInfo  = 'Operations addresses'   ! Get_Client_Emails(DEL:CID,2,1)     ! So we do have one, then replace default
    .    

    ! Then full in the general stuff
    LQ:ManifestInfo  = 'DI: ' & DEL:DINo & ' - ' & CLIP(LQ:ManifestInfo) & | 
                      '   for ' & CLIP(CLI:ClientName) & ' ' & CLIP(CLI:ClientNo) & ', ' & |
                      ' (' & MALD:UnitsLoaded & ' X ' & CLIP(COM:Commodity) & ')'
      
    LQ:TreeState      = 2
    LQ:EmailName      = EMAI:EmailName 
    LQ:EmailAddress   = EMAI:EmailAddress
    LQ:EAID           = EMAI:EAID
    LQ:DID            = DELI:DID    
    LQ:CID            = DEL:CID    
    LQ:Mark           = 0
    
    LQ:ManifestInfo_Style = 0
    ADD(LOC_Q)
    
    IF EMAI:Operations = TRUE 
      LQ:TreeState          = 3
      LQ:Mark               = 1
      LQ:ManifestInfo_Style = 2
      LQ:ManifestInfo       = EMAI:EmailAddress

      DO Check_Ops_Ref
      ADD(LOC_Q)
  . .

  DO Set_Marks_From_Q
  EXIT
      
      
Check_Ops_Ref       ROUTINE
  IF CLIP(EMAI:OperationsReference) <> ''
    ! we have a client ops ref 
    IF INSTRING(CLIP(EMAI:OperationsReference), CLIP(DEL:ClientReference),1,1)
      ! its in the delivery ref
      LQ:ManifestInfo       = CLIP(EMAI:EmailAddress) & '  (ops ref "' & CLIP(EMAI:OperationsReference) & '")'
    ELSE
      LQ:ManifestInfo       = CLIP(EMAI:EmailAddress) & '  (ops ref "' & CLIP(EMAI:OperationsReference) & '" not in client ref)'
      LQ:Mark               = 0
      LQ:ManifestInfo_Style = 0
  . .
  EXIT
!--------------------------------------------
Save_Mark_Q         ROUTINE
  ! Add all those without a Mark to this Queue
  
  FREE(LOC_Mark_Q)
  
  Idx_# = 0
  LOOP
    Idx_# += 1
    GET(LOC_Q, Idx_#)
    IF ERRORCODE()
      BREAK
    .
    
    ! 1st is emails separated second is did entries
    IF ((LQ:TreeState > 1 AND LQ:DID = 0) OR (LQ:TreeState > 2 AND LQ:DID ~= 0)) 
      L_MQ:DID          = LQ:DID
      L_MQ:EmailAddress = LQ:EmailAddress
      L_MQ:Mark         = LQ:Mark
      
      ADD(LOC_Mark_Q)
  . . 
  EXIT
  
Set_Marks_From_Q    ROUTINE
  Idx_# = 0
!  MESSAGE('q: ' & records(LOC_Mark_Q))
  LOOP
    Idx_# += 1
    GET(LOC_Mark_Q, Idx_#)
    IF ERRORCODE()
      BREAK
    .
    
    LQ:DID          = L_MQ:DID
    LQ:EmailAddress = L_MQ:EmailAddress
    GET(LOC_Q, LQ:DID, LQ:EmailAddress)        
    IF ~ERRORCODE()
      IF L_MQ:Mark ~= LQ:Mark        
        LQ:Mark = L_MQ:Mark
        
        EXECUTE LQ:Mark + 1
          LQ:ManifestInfo_Style = 0
          LQ:ManifestInfo_Style = 2
        .
          
        PUT(LOC_Q)
      . 
    ELSE    ! we dont have this record
      ! Doesnt exist.. so if normal address then add back in...
!        MESSAGE('not in q - did: ' & L_MQ:DID & ' : ' & L_MQ:EmailAddress) 
      IF L_MQ:DID = 0
        LO_EG:Email_Address = L_MQ:EmailAddress
        DO Add_Emails          
  . . . 
  EXIT
  
Style_Setup                ROUTINE
    ! ?LIST_ManifestEmails{PROPSTYLE:
    ?LIST_ManifestEmails{PROPSTYLE:TextColor, 2}     = -1
    ?LIST_ManifestEmails{PROPSTYLE:BackColor, 2}     = 0E9E9E9H
    ?LIST_ManifestEmails{PROPSTYLE:TextSelected, 2}  = COLOR:Blue
    ?LIST_ManifestEmails{PROPSTYLE:BackSelected, 2}  = 0E8E9E9H
    EXIT
Set_Tab                     ROUTINE
  EXECUTE CurrentTab + 1  
    SELECT(?TAB_Manifest)
    SELECT(?TAB_Content)
    SELECT(?TAB_Summary)
  ELSE
    BEGIN
      CurrentTab = 0
      SELECT(?TAB_Manifest)
  . .
  
  CASE CurrentTab
  OF 1
    ?STRING_Content{PROP:Text}  = 'There are ' & RECORDS(LOC_Add_Q) & ' unique email addresses to which the message will be sent.'
  OF 2
    ?PROMPT_Summary{PROP:Text}  = 'Email ready to send.  It will be sent to ' & RECORDS(LOC_Add_Q) & ' addresses.' 
  .
  
  EXIT
  
  
Save_Stuff                ROUTINE
  DATA
r:name_   STRING(6)

  CODE

  ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
  ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
  !   1       2       3           4           5

  ! ThisEmail.Server         = Get_Section_from_Settings(p_EmailAccount, 'Server', 'BC Domain Register SMTP Server')
  r:name_ = 'Email-'

  stuff_"         = Get_Setting(r:name_ & 'Server', 2, L_ESG:Server,, 'FBN Mailserver')

  stuff_" = Get_Setting(r:name_ & 'Account', 2, L_ESG:SMTP_Username, 'Email Account')
  stuff_" = Get_Setting(r:name_ & 'Password', 2, L_ESG:SMTP_Password, 'Email Account Password')
  
  stuff_"           = Get_Setting(r:name_ & 'From', 2, L_ESG:From, 'From Email address')
 !       ThisEmail.ToList         = Get_Setting(r:name_ & 'To', 'BC Domain Register Co ZA address')
  stuff_"         = Get_Setting(r:name_ & 'CC', 2, L_ESG:CC, 'CC Email address')
  stuff_" = Get_Setting(r:name_ & 'BCC', 2, L_ESG:BCC, 'BCC Email address')

  EXIT

  
Load_Stuff                  ROUTINE
  DATA
r:name_   STRING(6)

  CODE

  ! (p:Setting, p:Create, p:DefValue, p:Picture, p:Tip)
  ! (STRING, BYTE=0, <STRING>, <STRING>, <STRING>),STRING
  !   1       2       3           4           5

  ! ThisEmail.Server         = Get_Section_from_Settings(p_EmailAccount, 'Server', 'BC Domain Register SMTP Server')
  r:name_ = 'Email-'


  L_ESG:Server        = Get_Setting(r:name_ & 'Server', 1, 'mail.fbn-transport.co.za',, 'FBN Mailserver')

  L_ESG:SMTP_Username = Get_Setting(r:name_ & 'Account', 1, 'operations@fbn-transport.co.za', 'Email Account')
  L_ESG:SMTP_Password = Get_Setting(r:name_ & 'Password', 1, '', 'Email Account Password')
  
  L_ESG:From          = Get_Setting(r:name_ & 'From', 1, 'operations@fbn-transport.co.za', 'From Email address')
 !       ThisEmail.ToList         = Get_Setting(r:name_ & 'To', 'BC Domain Register Co ZA address')
  L_ESG:CC            = Get_Setting(r:name_ & 'CC', 1,'', 'CC Email address')
  L_ESG:BCC           = Get_Setting(r:name_ & 'BCC', 1,'', 'BCC Email address')
  EXIT
Add_Emails                ROUTINE
  CLEAR(LOC_Q)
  LQ:ManifestInfo = 'Additional Email Addresses'
  GET(LOC_Q, LQ:ManifestInfo)
  IF ERRORCODE()
    LQ:TreeState = 1
    ADD(LOC_Q)
  .

  CLEAR(LOC_Q)
  LQ:ManifestInfo = LO_EG:Email_Address
  GET(LOC_Q, LQ:ManifestInfo)
  IF ERRORCODE()
    LQ:TreeState          = 2
    LQ:EmailAddress       = LO_EG:Email_Address
    LQ:Mark               = 1
    LQ:ManifestInfo_Style = 2
    ADD(LOC_Q)
  .

  Check_Add_Q(0, '', LO_EG:Email_Address)      ! Add unique address to address q
  
  EXIT
  
!--------------------------------------------
Content_Add_Subs    ROUTINE             ! add place holder string to content
   !  ************         SETUP NOT USED HERE - SEE ManTranIS app
   
  in# = ?LO_EG:Content{PROP:Selected}   ! position to insert at
  
  LO_EG:Content   = SUB(LO_EG:Content,1,in#-1) & '[' & CLIP(LO_EG:Substitution) & ']'  & |
                      SUB(LO_EG:Content, in#, LEN(CLIP(LO_EG:Content)))
  
  DISPLAY(?LO_EG:Content)
  
  ?LO_EG:Content{PROP:SelStart} = in#  
  !SELECT(?LO_EG:Content, in#, in#+5)
  EXIT
  
!--------------------------------------------
!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
  !------------------------------------
  !Style for ?LIST_ManifestEmails
  !------------------------------------
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Manifest_Emails_Setup')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
      ! Option > 10 quiet email sends, various.. 
  IF p:Option >= 11
      IF TRI:TRID > 0
         LO_FB:TripSheet   = Access:TripSheets.SaveBuffer()
      .
      IF DEL:DID > 0
         LO_FB:Deliveries  = Access:Deliveries.SaveBuffer()
      .   
  
    !On Route|Transferred|Out On Delivery|Delivered|Manifest General
    CASE p:Option
    OF 11     ! Manifest ID passed
         LO_EG:Email_Type  = 'On Route'
         L_SG:Notice_Type = 1
    OF 12     ! Manifest ID passed
         LO_EG:Email_Type  = 'Transferred'
         L_SG:Notice_Type = 2
    OF 13     ! Tripsheet ID passed
         LO_EG:Email_Type  = 'Out On Delivery'
         L_SG:Notice_Type = 3
    OF 14     ! Tripsheet ID passed
         LO_EG:Email_Type  = 'Delivered' 
         L_SG:Notice_Type = 4
    OF 15
         LO_EG:Email_Type  = 'Manifest General'
         L_SG:Notice_Type = 5
    OF 16     ! Delivery ID passed
         LO_EG:Email_Type  = 'Delivered' 
         L_SG:Notice_Type = 4
    .  
    
  !  MESSAGE('LO_EG:Email_Type: ' & LO_EG:Email_Type)
    
    DO Load_Stuff
     
  !  LO_EG:Content = Load_Content_File(LO_EG:Email_Type)
  !  L_ESG:Subject = L_EG:Subject
  
    Load_Q_DIs()
  
     Send_Email_l(TRUE)
    
  !    POST(EVENT:CloseWindow)
    
      
    !ThisWindow.Kill()
    !RETURN 0
      ReturnValue = Level:Notify 
  .
    
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Cancel
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(Toolbar)
  SELF.AddItem(?Cancel,RequestCancelled)                   ! Add the cancel control to the window manager
  Relate:AddressAlias.SetOpenRelated()
  Relate:AddressAlias.Open                                 ! File AddressAlias used by this procedure, so make sure it's RelationManager is open
  Relate:NotificationsSetup.Open                           ! File NotificationsSetup used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:PackagingTypes.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TripSheetDeliveries.UseFile                       ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TripSheets.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Floors.UseFile                                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Addresses.UseFile                                 ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Commodities.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Deliveries.UseFile                                ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoadDeliveries.UseFile                    ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:ManifestLoad.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:EmailAddresses.UseFile                            ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailer.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:DeliveryItems.UseFile                             ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Clients.UseFile                                   ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
      IF TRI:TRID > 0
         LO_FB:TripSheet   = Access:TripSheets.SaveBuffer()
      .
      IF DEL:DID > 0
         LO_FB:Deliveries  = Access:Deliveries.SaveBuffer()
      .   
  
  
  SELF.Open(QuickWindow)                                   ! Open window
    ?STRING2{PROP:Text} = 'Manifest:  ' & p:ID
    ?LIST_ManifestEmails{PROP:LineHeight} = ?LIST_ManifestEmails{PROP:LineHeight} + 5
   
                                               ! Generated by NetTalk Extension (Start)
  ThisNetEmail.SuppressErrorMsg = 1         ! No Object Generated Error Messages ! Generated by NetTalk Extension
  ThisNetEmail.init()
  if ThisNetEmail.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  Do DefineListboxStyle
  !ProcedureTemplate = Window
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)      ! Controls like list boxes will resize, whilst controls like buttons will move
  SELF.AddItem(Resizer)                                    ! Add resizer to window manager
    PUSHBIND()
    BIND('EMAI:Operations',EMAI:Operations)
    DO Style_Setup                 
    DO Load_Q      
    LO_EG:Content   = Load_Content_File(LO_EG:Email_Type)
    LO_EG:Prev_Email_Type = LO_EG:Email_Type
    DISPLAY
  DO Load_Stuff
  LO_EG:Email_Type  = 'Manifest General'
  DISABLE(?LO_EG:Email_Type:2)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ThisNetEmail.Kill()                              ! Generated by NetTalk Extension
  IF p:Option < 11
    UNBIND('EMAI:Operations')
    POPBIND()
  .
  
  IF SELF.FilesOpened <> TRUE AND LO_FB:TripSheet <> 0
     Access:TripSheets.RestoreBuffer(LO_FB:TripSheet)
     Access:Deliveries.RestoreBuffer(LO_FB:Deliveries)
  .
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  IF LO_FB:TripSheet <> 0
      Access:TripSheets.RestoreBuffer(LO_FB:TripSheet)
   .
      IF LO_FB:Deliveries <> 0
         Access:Deliveries.RestoreBuffer(LO_FB:Deliveries)
      .
      
  
  
    Relate:AddressAlias.Close
    Relate:NotificationsSetup.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BUTTON_AddEmail
      ThisWindow.Update()
        IF ~INSTRING('@', LO_EG:Email_Address,1,1)
          MESSAGE('That doesn''t look like an email address.  There should be an @ sign in there somewhere.')
          SELECT(?LO_EG:Email_Address)
          CYCLE
        .
      
        DO Add_Emails
    OF ?BUTTON_ClientEmails
      ThisWindow.Update()
      IF CHOICE(?LIST_ManifestEmails) <= 0
        MESSAGE('Please select a record from the list first.')
        CYCLE
      ELSE
        GET(LOC_Q,  CHOICE(?LIST_ManifestEmails))
        CLI:CID = LQ:CID
        IF Access:Clients.TryFetch(CLI:PKey_CID) = Level:Benign
          GlobalRequest = ChangeRecord
          
          ! to be enabled when moved to Man app so we can call this
          Update_Clients(2)
          
          DO Load_Q      
        ELSE
          ! ??
        .
      .
      
    OF ?BUTTON_Exclude
      ThisWindow.Update()
        IF CHOICE(?LIST_ManifestEmails) <= 0
          LQ:TreeState = 3
          GET(LOC_Q, LQ:TreeState)
          SELECT(?LIST_ManifestEmails, POINTER(LOC_Q))
        .
          
        IF CHOICE(?LIST_ManifestEmails) <= 0
          MESSAGE('Please select a record from the list first.')
          CYCLE
        ELSE
          GET(LOC_Q,  CHOICE(?LIST_ManifestEmails))
          IF (LQ:TreeState < 3 AND LQ:DID ~= 0) OR (LQ:DID = 0 AND LQ:TreeState < 2)   ! then this is a root, mark all or un-mark all under it?  Or do nothing?
            ! nothing
            pt# = POINTER(LOC_Q)
            idx# = 0
            LQ:TreeState = 3
            GET(LOC_Q, LQ:TreeState)
            idx# = POINTER(LOC_Q)
            LOOP 
              IF (idx# > pt#) OR idx# = RECORDS(LOC_Q)          
                BREAK
              .        
              idx# += 1
              GET(LOC_Q, idx#)
              !MESSAGE('pointer: ' & POINTER(LOC_Q))        
            .
            IF LQ:TreeState = 3
              SELECT(?LIST_ManifestEmails, POINTER(LOC_Q))
            .      
          ELSE
            LQ:Mark = ABS(LQ:Mark - 1)              
            
            !MESSAGE('mark: ' & LQ:Mark) 
            EXECUTE LQ:Mark + 1
              LQ:ManifestInfo_Style = -1
              LQ:ManifestInfo_Style = 2
            .    
            
            PUT(LOC_Q)
            
            EXECUTE LQ:Mark + 1
              Check_Add_Q(LQ:DID, LQ:EmailName, LQ:EmailAddress, 1)      ! Add unique address to address q
              Check_Add_Q(LQ:DID, LQ:EmailName, LQ:EmailAddress, 0)      ! Add unique address to address q
            .
            
            DISPLAY()
            
            SELECT(?LIST_ManifestEmails, POINTER(LOC_Q))
            ! if the root of an entry then mark all records?
        . .
      
    OF ?BUTTON_LoadContent
      ThisWindow.Update()
      CASE MESSAGE('Any changes to the above will be lost.||Are you sure?','Content',ICON:Question,BUTTON:YES+BUTTON:NO,BUTTON:NO)
      OF BUTTON:YES
      !  LO_EG:Content = GETINI('Manifest_Emails','EmailContent','', GLO:Local_INI)
        LO_EG:Content = Load_Content_File(LO_EG:Email_Type)
        DISPLAY
      .
    OF ?BUTTON_SaveContent
      ThisWindow.Update()
      Save_Content_File(LO_EG:Email_Type)
      
      !PUTINI('Manifest_Emails','EmailContent', LO_EG:Content, GLO:Local_INI)
    OF ?BUTTON_Add
      ThisWindow.Update()
        DO Content_Add_Subs
    OF ?BUTTON_CopyClip
      ThisWindow.Update()
        SETCLIPBOARD(LO_EG:Content)
    OF ?Ok:2
      ThisWindow.Update()
        ! save stuff then send email
        DO Save_Stuff
        Save_Content_File(LO_EG:Email_Type)
      
        Send_Email_l()
        POST(EVENT:CloseWindow)     
    OF ?BUTTON_Next
      ThisWindow.Update()
      CurrentTab  += 1
      DO Set_Tab
    OF ?BUTTON_Back
      ThisWindow.Update()
      CurrentTab  -= 1
      DO Set_Tab
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisNetEmail.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all field specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?LIST_ManifestEmails
    CASE EVENT()
    OF EVENT:AlertKey
        POST(EVENT:Accepted, ?BUTTON_Exclude)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all NewSelection events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?SHEET1
        ! New tab is in focus
        EXECUTE CHOICE(?Sheet1)
          CurrentTab = 0
          CurrentTab = 1
          CurrentTab = 2
          CurrentTab = 3
        .
        DO Set_Tab
    OF ?LO_EG:Substitution:3
        DO Content_Add_Subs
    OF ?LO_EG:Email_Type:2
        ! Check if content has changed, if so then offer to save it
        IF CLIP(LO_EG:Content) ~= CLIP(Load_Content_File(LO_EG:Prev_Email_Type)) OR |
              CLIP(L_ESG:Subject) ~= CLIP(L_EG:Subject)
          
          CASE MESSAGE('Email subject/content has changed, would you like to save it?','Content',ICON:Question,BUTTON:Yes+BUTTON:NO)
          OF BUTTON:YES
            Save_Content_File(LO_EG:Prev_Email_Type)
          .
        .    
      
        LO_EG:Content   = Load_Content_File(LO_EG:Email_Type)
        L_ESG:Subject   = L_EG:Subject
        LO_EG:Prev_Email_Type   = LO_EG:Email_Type
      
        DISPLAY
          
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Load_Content_File   PROCEDURE(STRING p_Type)             ! Old code, not used by Silent stuff

mf FILE,DRIVER('ASCII','/FILEBUFFERS=8'), NAME('.\EmailContent.txt'),| 
         PRE(mf_),CREATE,THREAD
Record     RECORD,PRE()
Line        STRING(1024)
          END
    END

i_        LONG
l_txt     LIKE(LO_EG:Content)

  CODE
    ! Load subject
    L_EG:Subject  = Get_Setting('Email-' & CLIP(p_Type) & '-Subject', 1,'', 'Email subject')
    

    mf{PROP:Name} = CLIP(p_Type) & '.txt'
      
    OPEN(mf)
    IF ERRORCODE()
      ! no file yet
!      MESSAGE('No content file found?   ' & ERROR())
    ELSE
      CLEAR(l_txt)
      i_ = 0
      
      SET(mf)
      LOOP
        NEXT(mf)
        IF ERRORCODE()
!          MESSAGE('end of file?:  ' & ERROR() & '|||file: ' & mf{PROP:Name})
          BREAK
        .
        i_ += 1
        
!        MESSAGE('next: ' & ERROR() & '||line ' & i_ & ': ' & mf_:Line)

        IF i_ <= 1
          l_txt   = mf_:line 
        ELSE
          l_txt   = CLIP(l_txt) & '<13,10>' & CLIP(mf_:line)
        .
      .
      
      CLOSE(mf)    
    .
    
    L_EG:Content  = l_txt
  
  RETURN l_txt
  
Load_Content         PROCEDURE(p_NoticeStage, p_LoadType)
   CODE
   
   Loaded_# = 0
   NOT:NoticeStage   = p_NoticeStage
   NOT:LoadType      = p_LoadType
      
!      db.Debugout('==================== NOT:NoticeStage: ' & NOT:NoticeStage & '     NOT:LoadType: ' & NOT:LoadType)
      
   IF Access:NotificationsSetup.TryFetch(NOT:Key_Stage_Type) = Level:Benign
      IF NOT:DoNotGenerate = FALSE
         L_EG:Subject = NOT:Subject
         LO_EG:Content = NOT:Template
         Loaded_# = TRUE
!         db.Debugout('LO_EG:Content ' & CLIP(LO_EG:Content))
      .
   ELSE
      MESSAGE('Notice stage & type could not be loaded.  Stage: ' & NOT:NoticeStage & '  Type: ' & NOT:LoadType & '||Error: ' & ERRORCODE(), 'Notifications', ICON:Hand)
   .
   
   RETURN(Loaded_#)
Save_Content_File   PROCEDURE(STRING p_Type)

mf FILE,DRIVER('ASCII','/FILEBUFFERS=8'), NAME('.\EmailContent.txt'),| 
         PRE(mf_),CREATE,THREAD
Record     RECORD,PRE()
Line        STRING(1024)
          END
         END
i_                    LONG


  CODE
    stuff_" = Get_Setting('Email-' & CLIP(p_Type) & '-Subject', 2, L_ESG:Subject, 'Email subject')


    COPY(CLIP(p_Type) & '.txt', CLIP(p_Type) & FORMAT(TODAY(),@d6_)  & FORMAT(CLOCK(),@t1_) & '.txt')   ! a backup

    mf{PROP:Name} = CLIP(p_Type) & '.txt'
    
    CREATE(mf)
    OPEN(mf)
    IF ERRORCODE()
      MESSAGE('On Save - Open file error: ' & Error())
    ELSE
      mf_:Line  = CLIP(LO_EG:Content)
      
      ADD(mf)
      IF ERRORCODE()
        MESSAGE('On Save - Add error: ' & Error())
      .
      
      CLOSE(mf)
    .
    DISPLAY
  RETURN
  
!--------------------------------------------
Check_Add_Q         PROCEDURE(pDID, pEmailName, pEmailAddress, pOption)
  CODE
    
  IF pOption = 0
    ! Add unique address to address q
    L_AQ:DID          = pDID
    L_AQ:EmailName    = pEmailName
    L_AQ:EmailAddress = pEmailAddress
    
    GET(LOC_Add_Q, L_AQ:DID, L_AQ:EmailAddress)    ! Unique Addresses per DI
    IF ERRORCODE()      
      ADD(LOC_Add_Q)      
    .
  ELSE
    ! delete from Q
    L_AQ:EmailAddress = pEmailAddress
    
    GET(LOC_Add_Q, L_AQ:DID, L_AQ:EmailAddress)    ! Unique Addresses
    IF ~ERRORCODE()      
      DELETE(LOC_Add_Q)
  . .
  RETURN
!--------------------------------------------
Merge_Content       PROCEDURE(ULONG p:DID, STRING p:Content)
L:Content             LIKE(LO_EG:Content)

L:Count               LONG(0)
L:Commodities         STRING(255)
L:Packaging_S         STRING(255)
L:Units               LONG
L:Weight              DECIMAL(12)
                      
Del_View              VIEW(Deliveries)
                           PROJECT(DEL:DID,DEL:DINo,DEL:ClientReference,DEL:CollectionAID,DEL:DeliveryAID,DEL:Notes, |
                              DEL:ReceivedDate,DEL:ReceivedTime)
                        JOIN(DELI:FKey_DID_ItemNo,DEL:DID)
                          PROJECT(DELI:Units,DELI:Weight,DELI:CMID)
                          JOIN(COM:PKey_CMID, DELI:CMID)
                              PROJECT(COM:Commodity)
                          .
                          JOIN(PACK:PKey_PTID, DELI:PTID)
                            PROJECT(PACK:Packaging)
                        . .
                        JOIN(CLI:PKey_CID, DEL:CID)
                          PROJECT(CLI:ClientName)
                        .
                        JOIN(FLO:PKey_FID, DEL:FID)
                          PROJECT(FLO:Floor)
                      . .

View_Del              ViewManager

L:TripSheets          STRING(150)
L:TS_Veh              STRING(100)
L:BranchTel           STRING(100)
L:Del_Date            STRING(100)

   CODE
   L:Content = p:Content
     
   View_Del.Init(Del_View, Relate:Deliveries)
   View_Del.AddSortOrder(DEL:PKey_DID)
   View_Del.AppendOrder('DELI:ItemNo')
   View_Del.AddRange(DEL:DID, p:DID)
   !View_Del.SetFilter()  
   View_Del.Reset()    
   LOOP
     IF View_Del.Next() ~= LEVEL:Benign        ! Failed to fetch the DI.. 
       BREAK      
     ELSE    
       L:Count += 1
       
       IF L:Count = 1                          ! some fields we only have one of
         !  Client Name|Collection Add|Delivery Add|Client Ref|Commodity|Items|Weight|Special Remarks|ETA|Branch|
         !        Dispatch Vehicle|Delivery Date/Time|Branch Tel.|DI No.|Packaging
         ! (p:String, p:Find, p:Replace, p:Occurances, p:No_Case)
         ! (*STRING, STRING, STRING, LONG=0, BYTE=0), LONG
         Replace_Strings(L:Content, '[Client Name]', CLIP(CLI:ClientName))
         Replace_Strings(L:Content, '[Collection Add]', CLIP(Get_Address(DEL:CollectionAID,1,1)))
         Replace_Strings(L:Content, '[Delivery Add]', CLIP(Get_Address(DEL:DeliveryAID,1,1)))
         Replace_Strings(L:Content, '[Client Ref]', CLIP(DEL:ClientReference))
         
!         db.Debugout('_____________________________________________________ +')
!         db.Debugout('+: ' & L:Content)
         
         Replace_Strings(L:Content, '[DI No.]', CLIP(DEL:DINo))
         
!         db.Debugout('-: ' & L:Content)
!         db.Debugout('_____________________________________________________ -')

         Replace_Strings(L:Content, '[Special Remarks]', CLIP(DEL:Notes))
           
         !Branch|Dispatch Vehicle|Delivery Date/Time
         ! Branch is the DI FID name
         Replace_Strings(L:Content, '[Branch (DI)]', CLIP(FLO:Floor))
            
         ! Check for a TripSheet delivery
         TRDI:DIID = DELI:DIID        
         IF Access:TripSheetDeliveries.TryFetch(TRDI:FKey_DIID) = Level:Benign
            ! We have at least one, use this one for now!     
            L:Del_Date  = FORMAT(TRDI:DeliveredTime, @t1) & ' ' & FORMAT(TRDI:DeliveredDate,@d6)
       . .
       
       Add_to_List(CLIP(COM:Commodity), L:Commodities, ', ')
       Add_to_List(CLIP(PACK:Packaging), L:Packaging_S, ', ')
       L:Units   += DELI:Units      
       L:Weight  += DELI:Weight        ! do they want volumetric?
     .
   .
   View_Del.Kill()    

   Replace_Strings(L:Content, '[Commodity]', CLIP(L:Commodities))
   Replace_Strings(L:Content, '[Packaging]', CLIP(L:Packaging_S))      
   Replace_Strings(L:Content, '[Items]', L:Units)
   Replace_Strings(L:Content, '[Weight]', L:Weight)
   
   
   ! Journey - if we have a Overnight load, then the Journey should give the opposite branch, DBN / JHB to the DI Branch   
   IF L_SG:Load_Type = 1 
      ! We should have this data then L_SG:Destination_BranchName - see Load_Q_DIs -> InitMan
      Replace_Strings(L:Content, '[Branch Journey-TO (M)]', L_SG:Destination_BranchName)      
   .

   ! Get delivery vehicle info
   ! Returns comma del list of tripsheets in first to last order
   L:TripSheets  = Get_Delivery_TripSheets(DEL:DID)
   TRI:TRID      = Get_1st_Element_From_Delim_Str(L:TripSheets,',',1)
   IF Access:TripSheets.TryFetch(TRI:PKey_TID) = LEVEL:Benign
       ! p:Option  1.  Capacity                            of all
       !           2.  Registrations                       of all OR p:Truck = 100 gives horse reg only
       !           3.  Makes & Models                      of all
       !           4.  Description of Truck / Trailer      requires p:Truck  0 to 3
       !           5.  Truck / Trailer Capacity            requires p:Truck  0 to 3
       !           6.  Truck / Trailer ID (TTID)           requires p:Truck  0 to 3
       !           7.  Count of vehicles in composition
       !           8.  Compositions Capacity
       !           9.  Driver of Horse
       !           10. Transporters ID for this VCID
       !           11. Licensing dates                     of all
       L:TS_Veh    = Get_VehComp_Info(TRI:VCID, 2)
       L:BranchTel = Get_Address(Get_Branch_Info(TRI:BID, 2), 3)   ! could return comma del list      
   . 
     
   Replace_Strings(L:Content, '[Dispatch Vehicle (TS)]', CLIP(L:TS_Veh))     ! Trip sheet  
   Replace_Strings(L:Content, '[Branch Tel. (TS)]', CLIP(L:BranchTel))  

   ! see above in loop
   Replace_Strings(L:Content, '[Delivery Date/Time (TS)]', CLIP(L:Del_Date))

   !t#  = DEFORMAT('13:00', @t1)     ! hh:mm      
   !Date_Time_Advance(MAN:DepartDate, MAN:DepartTime, 1, t#)         ! CANT change MANIFEST Info here
   Replace_Strings(L:Content, '[ETA (M)]', FORMAT(MAN:ETADate,@d6) & ' @ ' & FORMAT(MAN:ETATime, @t1))    

   RETURN(L:Content)
     
     ! Date_Time_Advance
     ! (*DATE, <*TIME>, BYTE, LONG)
     ! (p_Date, p_Time, p_Option, p_Val)
     !
     ! p_Option
     !       1 - Time
     !       2 - Days
     !       3 - Weeks
     !       4 - Months
     !       5 - Quarters
     !       6 - Years
     
     ! Get_Address
     ! (p:AID, p:Type, p:Details)
     ! (ULONG, BYTE=0, BYTE=0),STRING
     ! p:Type
     !   0 - Comma delimited string
     !   1 - Block
     !   2 - Comma delimited string - including empty Line 1 & 2 where they are empty
     !   3 - Phone Nos. (comma delim)
     ! p:Details
     !   0 - all
     !   1 - without City & Province
     !
     ! Returns
     !   Address Name, Line1 (if), Line2 (if), Post Code, City, Province
     !   or
     !   Address Name, Line1 (if), Line2 (if), Post Code
     !   or
     !   Address Name, Line1 (if), Line2 (if), <missing info string>
     !   or
     !   Phone1, Phone2 OR Phone1 OR Phone 2
!--------------------------------------------
Load_Q_DIs              PROCEDURE()
cl    Load_Q_Class
  CODE
  IF p:Option = 11 OR p:Option = 12
    cl.InitMan()
  ELSIF p:Option = 13 OR p:Option = 14
    cl.InitTrip()
  ELSIF p:Option = 16
    DEL:DID = p:ID
    IF Access:Deliveries.TryFetch(DEL:PKey_DID) = Level:Benign   

       ! *********************************************   
       L_SG:Load_Type = 1         ! Overnight
       !IF MAN:Broking = TRUE      ! NO MANIFEST
       !   L_SG:Load_Type = 2
       !.  
       Load_Content(L_SG:Notice_Type, L_SG:Load_Type)
       L_ESG:Subject = L_EG:Subject
       ! *********************************************   

       IF CLIP(DEL:NoticeEmailAddresses) <> ''
          Check_Add_Q(DEL:DID, '', DEL:NoticeEmailAddresses)      ! Add unique address to address q LOC_Add_Q
    .  .
  .    

  IF p:Option = 16
     !     nothing 
  ELSE
     cl.Loop_()        
  .   
  RETURN
  
   
Load_Q_Class.InitTrip   PROCEDURE()
  CODE
  IF TRI:TRID ~= p:ID
    CLEAR(TRI:Record)
    TRI:TRID = p:ID
    IF Access:TripSheets.TryFetch(TRI:PKey_TID) ~= Level:Benign
      ! return??
  . . 
      
  ! *********************************************   
  L_SG:Load_Type = 1         ! Overnight
  !IF MAN:Broking = TRUE      ! NO MANIFEST
  !   L_SG:Load_Type = 2
  !.  
  Load_Content(L_SG:Notice_Type, L_SG:Load_Type)
  L_ESG:Subject = L_EG:Subject
  ! *********************************************   

  SELF.VM_DD.Init(View2_, Relate:TripSheetDeliveries)
  SELF.VM_DD.AddSortOrder(TRDI:FKey_TRID)
  SELF.VM_DD.AppendOrder('CLI:ClientName,DEL:DINo')
  !SELF.VM_DD.SetFilter('EMAI:Operations = 1')
  SELF.VM_DD.AddRange(TRDI:TRID,p:ID)
  SELF.VM_DD.Reset()
  IF ERRORCODE()
    MESSAGE('Error on view trip sheet reset: ' & ERROR())
  .
  RETURN

Load_Q_Class.InitMan    PROCEDURE()
CODE
   db.Debugout('---------------- init man - p:ID: ' & p:ID & '     + MAN:MID: ' & MAN:MID)
   IF MAN:MID ~= p:ID
     CLEAR(MAN:Record)
     MAN:MID = p:ID
     IF Access:Manifest.TryFetch(MAN:PKey_MID) ~= Level:Benign
         MESSAGE('Failed to load the Manifest: ' & p:ID & '||Please contact support with this error: ' & ERROR())
  .  .
     
   Floor_# = Get_Branch_Info(MAN:BID, 3)
   JOU:JID = MAN:JID
   IF Access:Journeys.TryFetch(JOU:PKey_JID) = Level:Benign
   .

   ! *********************************************   
   L_SG:Load_Type = 1
   IF MAN:Broking = TRUE   
      L_SG:Load_Type = 2
   .         
   ! Local deliveries SAME as Direct, and it means Journey has this MAN:BID branches floor and no destination Floor
   IF Floor_# = JOU:FID AND JOU:FID2 = 0
      ! Manifest branches Floor is Journeys Origin Floor, and no destination Floor = Direct
      L_SG:Load_Type = 3
   .
   
   ! Manifest Overnight
   IF L_SG:Load_Type = 1
      ! Manifest BrandID Floor is same as Journey Source Floor
      IF Floor_# = JOU:FID
         ! Then Journey destination Floor is Journey Floor 2
         L_SG:Destination_BranchName = Get_Floor_Info(JOU:FID2, 102)
      ELSE
         L_SG:Destination_BranchName = Get_Floor_Info(JOU:FID, 102)
      .       
   .
         
   Load_Content(L_SG:Notice_Type, L_SG:Load_Type)
   L_ESG:Subject = L_EG:Subject
   ! *********************************************   
   
   SELF.VM_DD.Init(View_, Relate:ManifestLoad)
   SELF.VM_DD.AddSortOrder(MAL:FKey_MID)
   SELF.VM_DD.AppendOrder('MAL:MLID,CLI:ClientName,DEL:DINo')
   !SELF.VM_DD.SetFilter('EMAI:Operations = 1')
   SELF.VM_DD.AddRange(MAL:MID,p:ID)
   SELF.VM_DD.Reset()
   IF ERRORCODE()
     MESSAGE('Error on view man reset: ' & ERROR())
   .
   RETURN
    

Load_Q_Class.Loop_   PROCEDURE()
  CODE  
   LOOP
     IF SELF.VM_DD.Next() ~= Level:Benign
       BREAK
     .
     !  db.Debugout('---------------- loop - DEL:DID: ' & DEL:DID)
     IF CLIP(DEL:NoticeEmailAddresses) <> ''
       Check_Add_Q(DEL:DID, '', DEL:NoticeEmailAddresses)      ! Add unique address to address q LOC_Add_Q
   . .
   !SELF.VM_DD.Close()
   SELF.VM_DD.Kill()  
   RETURN
      
    
Load_Q_Class.Construct  PROCEDURE()
  CODE
  FREE(LOC_Add_Q)  
  SELF.VM_DD   &= NEW(ViewManager)
  RETURN
    
Load_Q_Class.Destruct  PROCEDURE()
  CODE
  DISPOSE(SELF.VM_DD)
  RETURN
!--------------------------------------------
Send_Email_l                PROCEDURE(BYTE p_Silent=0)

r:idx          LONG
r:send_fails   LONG
r:content      LIKE(LO_EG:Content)
r:subject      LIKE(L_ESG:Subject)

r:result       STRING(20)
!r:stuff       STRING(1000)
r:retry_failed                   BYTE(0)
r:Col          LONG(6)
r:Existing_Row LONG(-1)          ! -1 makes a new row

CODE
   ! Save all the stuff you want to save here
   IF RECORDS(LOC_Add_Q) <= 0
      IF p_Silent = FALSE
         MESSAGE('There aren''t any email addresses to send to.')
      .
   ELSE      
      LOOP          
         r:idx = 0
         LOOP
            r:idx += 1
            GET(LOC_Add_Q, r:idx)
            IF ERRORCODE()
              BREAK
            .
            IF r:retry_failed = TRUE               ! Check that it was a failed one in the queue
               r:Existing_Row = R_Result_Q.Find_Row(L_AQ:DID & '-' & CLIP(L_AQ:EmailAddress), r:Col) 
               IF r:Existing_Row <= 0 
                  CYCLE
            .  .

            r:content = Merge_Content(L_AQ:DID, LO_EG:Content)
            r:subject = Merge_Content(L_AQ:DID, L_ESG:Subject)
            IF Get_Setup_Info(6) > 0    
               r:subject = '[TEST SYS] ' & r:subject
            .
            ! (ULONG p:ID,BYTE p:Option,BYTE p:Debug,<STRING p:DebugTo>)
            IF p:Debug > 0
!      db.Debugout('***** ' & p:Debug & '  -- ' & p:DebugTo)
               L_AQ:EmailAddress = p:DebugTo
               L_ESG:CC = ''
               L_ESG:BCC = ''
            .
      
!      db.Debugout('***** L_ESG:Subject:  '& CLIP(L_ESG:Subject                  ))
!               db.Debugout('***** r:subject: '& CLIP(r:subject)                  )
!               db.Debugout('***** LO_EG:Content: '& CLIP(LO_EG:Content)                  )
!               db.Debugout('##### L_AQ:EmailAddress: ' & CLIP(L_AQ:EmailAddress))
               

            ! (p_Subject, p_Text, p_EmailAccount, p_HTML, p_Server, p_AuthUser, p_AuthPassword, p_From, p_To, p_CC, p_BCC, 
            ! p_Helo, p_Attach)
            !    MESSAGE('add: ' & CLIP(L_AQ:EmailAddress))
            r:result    = Send_Email(r:subject, r:content, 0,, L_ESG:Server, L_ESG:SMTP_Username, L_ESG:SMTP_Password |
                              , L_ESG:From, CLIP(L_AQ:EmailAddress), L_ESG:CC, L_ESG:BCC)

               R_Result_Q.Set_Row('pending, DID: ' & L_AQ:DID & ',' & CLIP(L_AQ:EmailAddress), r:Existing_Row)
            R_Result_Q.Set_Row_Item(4, 'Subject: ' & r:Subject)
!            db.Debugout('-------------------- R_Result_Q.Rows: ' &  R_Result_Q.Rows &  '      r:Existing_Row: ' & r:Existing_Row)
            IF r:result = ''                          ! Success 
               R_Result_Q.Set_Row_Item(1, 'Success')
!               r:send_fails += 1    ! for testingnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
!               R_Result_Q.Set_Row_Item(6, L_AQ:DID & '-' & CLIP(L_AQ:EmailAddress))    ! Our ID row for possible re-sends
            ELSE            
               R_Result_Q.Set_Row_Item(1, 'Failed')            
               R_Result_Q.Set_Row_Item(5, CLIP(r:result))
               R_Result_Q.Set_Row_Item(6, L_AQ:DID & '-' & CLIP(L_AQ:EmailAddress))    ! Our ID row for possible re-sends
               r:send_fails += 1
               
               R_Error_Q.Set_Row('Failed, DID: ' & L_AQ:DID & ',' & CLIP(L_AQ:EmailAddress) & ',Subject: ' & r:Subject & CLIP(r:result))               
         .  .
         
         IF r:send_fails > 0
            CASE MESSAGE(r:send_fails & ' out of the ' & RECORDS(LOC_Add_Q) & ' emails failed to send.||Would you like to try failed emails again?' |
               & '||Failed emails:|' & R_Result_Q.List_Rows(',  ') ,'Email Sending',ICON:Exclamation , BUTTON:Yes+BUTTON:NO,BUTTON:NO)
            OF BUTTON:YES
               r:retry_failed = TRUE
               r:send_fails   = 0
            ELSE
               r:retry_failed = FALSE
               BREAK
            .
         ELSE
            IF p_Silent = FALSE
               MESSAGE('Email sending complete.||Sent to:|' & R_Result_Q.List_Rows(',  ', 5))
            .
            BREAK
      .  .
      Add_Log(R_Result_Q.List_Rows(',  '), 'Results - ' & CLIP(GLO:Login), 'Email_Notifications.Log')               
      IF r:send_fails > 0
         Add_Log(R_Error_Q.List_Rows(',  '), 'Errors - ' & CLIP(GLO:Login), 'Email_Notification_Errors.Log')                           
   .  .
   RETURN

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults()                                 ! Calculate default control parent-child relationships based upon their positions on the window
  SELF.SetStrategy(?BUTTON_ClientEmails, Resize:FixRight, Resize:LockSize) ! Override strategy for ?BUTTON_ClientEmails

