

   MEMBER('WorTrnis.clw')                                  ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WORTRNIS003.INC'),ONCE        !Local module procedure declarations
                     END


!!! <summary>
!!! Generated from procedure template - Window
!!! </summary>
Update_Manifest_Wizard PROCEDURE 

LOC:Cfg_LastTabNo    BYTE(5)                               !
LOC:Screen_Data      GROUP,PRE(L_SD)                       !
Tonnage              GROUP,PRE(L_SD)                       !
Link_cost            DECIMAL(7,2,5000)                     !
Tri_cost             DECIMAL(7,2,4000)                     !
Ton15_cost           DECIMAL(7,2,3500)                     !
Ton8_cost            DECIMAL(7,2)                          !
Ton5_cost            DECIMAL(7,2)                          !
Ton2_cost            DECIMAL(7,2,3000)                     !
                     END                                   !
Capacity             BYTE                                  !33 Tons, 22 Tons, 15 Tons, 2 Tons
TransporterName      STRING(35)                            !Transporters Name
Horse_Registration   STRING(20)                            !
Trailer1_Reg         STRING(20)                            !
Trailer2_Reg         STRING(20)                            !
NewCompositionName   STRING(35)                            !
Journey              CSTRING(701)                          !Description
Driver               STRING(70)                            !
CompositionName      STRING(35)                            !
                     END                                   !
LOC:Data             GROUP,PRE(L_D)                        !
TID                  ULONG                                 !Transporter ID
TTID_FreightLiner    ULONG                                 !Track or Trailer ID
TTID_Trailer         ULONG                                 !Track or Trailer ID
TTID_SuperLink       ULONG                                 !Track or Trailer ID
VCID                 ULONG                                 !Vehicle Composition ID
JID                  ULONG                                 !Journey ID - note that once a journey has been used it should not be changed, the collection and delivery addresses should also not be able to change
DRID                 ULONG                                 !Drivers ID
Broking              BYTE(0)                               !Broking Manifest
                     END                                   !
Default_Group        GROUP,PRE(L_DGG)                      !
Default_TID          ULONG                                 !Transporter ID
                     END                                   !
Window               WINDOW('New Manifest'),AT(,,495,320),FONT('Microsoft Sans Serif',8,,FONT:regular),DOUBLE,GRAY, |
  MDI,SYSTEM
                       SHEET,AT(6,20,483,276),USE(?SHEET1)
                         TAB('Select Capacity'),USE(?TAB_Capacity)
                           BUTTON('Link  -  33 Tons (2 trailers)'),AT(89,59,132,22),USE(?BUTTON_Link33),FONT(,10),LEFT
                           BUTTON('Tri Axle  -  22 Tons (1 trailer)'),AT(89,89,132,22),USE(?BUTTON_Tri),FONT(,10),LEFT
                           BUTTON('15 Tonner  -  15 Tons'),AT(89,120,132,22),USE(?BUTTON_15Ton),FONT(,10),LEFT
                           BUTTON('8 Tonner -  8 Tons'),AT(89,151,132,22),USE(?BUTTON_8Ton),FONT(,10),LEFT
                           BUTTON('5 Tonner -  5 Tons'),AT(89,182,132,22),USE(?BUTTON_5Ton),FONT(,10),LEFT
                           BUTTON('2 Tonner  -  2 Tons'),AT(89,212,132,22),USE(?BUTTON_2Ton),FONT(,10),LEFT
                           ENTRY(@n-10.2),AT(246,64,60,10),USE(L_SD:Link_cost),FONT(,10),RIGHT(1)
                           ENTRY(@n-10.2),AT(246,95,60,10),USE(L_SD:Tri_cost),FONT(,10),RIGHT(1)
                           ENTRY(@n-10.2),AT(246,126,60,10),USE(L_SD:Ton15_cost),FONT(,10),RIGHT(1)
                           ENTRY(@n-10.2),AT(246,156,60,10),USE(L_SD:Ton8_cost),FONT(,10),RIGHT(1)
                           ENTRY(@n-10.2),AT(246,187,60,10),USE(L_SD:Ton5_cost),FONT(,10),RIGHT(1)
                           ENTRY(@n-10.2),AT(246,218,60,10),USE(L_SD:Ton2_cost),FONT(,10),RIGHT(1)
                           PROMPT('Cost'),AT(245,43,61),USE(?PROMPT1),FONT(,10),CENTER,TRN
                           PROMPT('Selected'),AT(319,63,58),USE(?PROMPT_Selected),FONT(,,003C14DCh,FONT:bold),CENTER, |
  HIDE,TRN
                         END
                         TAB('Select Transporter'),USE(?TAB_Transporter)
                           PROMPT('Transporter Name:'),AT(25,58,,12),USE(?L_SD:TransporterName:Prompt),FONT(,10),TRN
                           ENTRY(@s35),AT(106,58,173),USE(L_SD:TransporterName),FONT(,10),MSG('Transporters Name'),REQ, |
  TIP('Transporters Name')
                           BUTTON('...'),AT(283,58,18,15),USE(?CallLookup),FONT(,10)
                         END
                         TAB('Select Horse'),USE(?TAB_Select_Vehicle),FONT(,8)
                           PROMPT('Composition Name:'),AT(27,58),USE(?L_SD:CompositionName:Prompt),TRN
                           ENTRY(@s35),AT(111,58,173,14),USE(L_SD:CompositionName),REQ
                           BUTTON('...'),AT(289,58,19,15),USE(?CallLookup:5)
                           GROUP('group3'),AT(20,94,265),USE(?GROUP_Horse),TRN
                             PROMPT('Horse Registration:'),AT(27,103),USE(?L_SD:Horse_Registration:Prompt),FONT(,10),TRN
                             ENTRY(@s20),AT(111,103,102,14),USE(L_SD:Horse_Registration),FONT(,10)
                             BUTTON('...'),AT(219,103,19,15),USE(?CallLookup:2),FONT(,10)
                           END
                           PROMPT('Driver:'),AT(27,156),USE(?L_SD:Driver:Prompt),TRN
                           ENTRY(@s70),AT(111,156,102,14),USE(L_SD:Driver)
                           BUTTON('...'),AT(219,156,19,15),USE(?CallLookup:4)
                         END
                         TAB('Select Trailers'),USE(?TAB_Select_Trailers)
                           PROMPT('Trailer 1 Registration:'),AT(35,58),USE(?L_SD:Trailer1_Reg:Prompt:3),FONT(,10),TRN
                           ENTRY(@s20),AT(128,58,102),USE(L_SD:Trailer1_Reg,,?L_SD:Trailer1_Reg:2),FONT(,10)
                           BUTTON('...'),AT(235,58,19,15),USE(?CallLookup_Trailer1),FONT(,10)
                           GROUP('group1'),AT(28,84,231),USE(?GROUP_Trailer2),TRN
                             PROMPT('Trailer 2 Registration:'),AT(35,95),USE(?L_SD:Trailer2_Reg:Prompt),FONT(,10),TRN
                             ENTRY(@s20),AT(128,95,102),USE(L_SD:Trailer2_Reg),FONT(,10)
                             BUTTON('...'),AT(235,94,19,15),USE(?CallLookup_Trailer2),FONT(,10)
                           END
                         END
                         TAB('Journey'),USE(?TAB_Journey)
                           ENTRY(@s255),AT(81,58,398,14),USE(L_SD:Journey),FONT(,10),MSG('Description'),REQ,TIP('Description')
                           PROMPT('Journey:'),AT(40,58),USE(?L_SD:Journey:Prompt),FONT(,10),TRN
                           BUTTON('...'),AT(81,76,19,15),USE(?CallLookup:3),FONT(,10,,FONT:regular)
                         END
                         TAB('Finish'),USE(?TAB_Finish)
                           TEXT,AT(36,49,423,163),USE(?TEXT_Fin),FONT(,12)
                           PROMPT('New Composition Name:'),AT(38,223),USE(?L_SD:NewCompositionName:Prompt),TRN
                           ENTRY(@s35),AT(121,223,152,10),USE(L_SD:NewCompositionName),REQ
                         END
                       END
                       BUTTON('&Next'),AT(393,258,87,28),USE(?BUTTON_Next),FONT(,14)
                       BUTTON('&Back'),AT(302,258,87,28),USE(?BUTTON_Back),FONT(,14)
                       BUTTON('&Cancel'),AT(424,297,63,19),USE(?CancelButton:2),LEFT,ICON('WACancel.ico')
                       GROUP,AT(40,297,337,21),USE(?GROUP_Info)
                         PROMPT('Debug Info'),AT(41,308,335),USE(?PROMPT_DBG),TRN
                         PROMPT('JID:'),AT(41,299),USE(?L_D:JID:Prompt)
                         STRING(@n_10),AT(59,299),USE(L_D:JID),RIGHT(1)
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Cls                  CLASS

WizardTab               BYTE
Trailers                BYTE
FromTabToFinish         BYTE
Cost                    DECIMAL(14,2)
ActualCapacity          DECIMAL(14,2)
CompositionChosen       BYTE

Next                    PROCEDURE(SIGNED p_Field=0)
Back                    PROCEDURE()

p_Field_Process         PROCEDURE(SIGNED p_Field)

SetTab                  PROCEDURE(LONG p_Tab)
UpdTab                  PROCEDURE(LONG l_NextTab)
Finsihed                PROCEDURE()

Construct               PROCEDURE()
Destruct                PROCEDURE()

GetComposition          PROCEDURE(ULONG)

Check_Finish_Ready      PROCEDURE()
Check_Vehicle_Composition     PROCEDURE()
Check_Driver            PROCEDURE(ULONG p_TID)

CompositionChosen       PROCEDURE(ULONG p:VCID)
Set_Trucks              PROCEDURE()

                     .
_SQL                 SQLQueryClass

   
   

EQU:CRLF            EQUATE('<13,10>')

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Manifest_Wizard')
  _SQL.Init(GLO:DBOwner, '__SQLTemp2')
  L_D:TID  = GETINI('Manifest', 'Default_Manifest_TransID14', 2, GLO:Local_INI)
  IF L_D:TID <> 0
     TRA:TID  = L_D:TID
     IF Access:Transporter.TryFetch(TRA:PKey_TID) = Level:Benign
        L_SD:TransporterName    = TRA:TransporterName
     .
     L_DGG:Default_TID    = L_D:TID
  .
  
  
  
  L_D:JID  = GETINI('Manifest', 'Default_Manifest_JID14', 1, GLO:Local_INI)     ! changed ini to 14 so users all get new default!
  IF L_D:JID <> 0
     JOU:JID = L_D:JID
     IF Access:Journeys.TryFetch(JOU:PKey_JID) = Level:Benign
        L_SD:Journey   = JOU:Journey
     .
  .
  
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?BUTTON_Link33
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  Relate:Drivers.SetOpenRelated()
  Relate:Drivers.Open                                      ! File Drivers used by this procedure, so make sure it's RelationManager is open
  Access:Journeys.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailerAlias.UseFile                         ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Manifest.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:TruckTrailer.UseFile                              ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:VehicleComposition.UseFile                        ! File referenced in 'Other Files' so need to inform it's FileManager
  Access:Transporter.UseFile                               ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(Window)                                        ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Update_Manifest_Wizard',Window)            ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  ?SHEET1{PROP:Wizard}    = TRUE
   LOC:Cfg_LastTabNo   = 5
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:Drivers.Close
  END
  IF SELF.Opened
    INIMgr.Update('Update_Manifest_Wizard',Window)         ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled                         ! Always return RequestCancelled if the form was opened in ViewRecord mode
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Select_Transporter
      Select_VehicleComposition
      Browse_TruckTrailer(0)
      Browse_Drivers('','')
      Browse_TruckTrailer(1,2)
      Browse_TruckTrailer(1,2)
      Browse_Journeys
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?CancelButton:2
      CASE MESSAGE('Would you like to cancel?','Cancel this Manifest',ICON:Question,BUTTON:YES+BUTTON:NO,BUTTON:NO)
      OF BUTTON:Yes
         POST(EVENT:CloseWindow)
      OF BUTTON:No
         !Window(PROP:AcceptAll) = FALSE
         SELECT(?SHEET1)    
      .
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BUTTON_Link33
      ThisWindow.Update()
      Cls.Next(ACCEPTED())
    OF ?BUTTON_Tri
      ThisWindow.Update()
      Cls.Next(ACCEPTED())
    OF ?BUTTON_15Ton
      ThisWindow.Update()
      Cls.Next(ACCEPTED())
    OF ?BUTTON_8Ton
      ThisWindow.Update()
      Cls.Next(ACCEPTED())
    OF ?BUTTON_5Ton
      ThisWindow.Update()
      Cls.Next(ACCEPTED())
    OF ?BUTTON_2Ton
      ThisWindow.Update()
      Cls.Next(ACCEPTED())
    OF ?L_SD:TransporterName
      IF L_SD:TransporterName OR ?L_SD:TransporterName{PROP:Req}
        TRA:TransporterName = L_SD:TransporterName
        IF Access:Transporter.TryFetch(TRA:Key_TransporterName)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            L_SD:TransporterName = TRA:TransporterName
            L_D:TID = TRA:TID
            L_D:Broking = TRA:Broking
          ELSE
            CLEAR(L_D:TID)
            CLEAR(L_D:Broking)
            SELECT(?L_SD:TransporterName)
            CYCLE
          END
        ELSE
          L_D:TID = TRA:TID
          L_D:Broking = TRA:Broking
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup
      ThisWindow.Update()
      TRA:TransporterName = L_SD:TransporterName
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        L_SD:TransporterName = TRA:TransporterName
        L_D:TID = TRA:TID
        L_D:Broking = TRA:Broking
      END
      ThisWindow.Reset(1)
    OF ?L_SD:CompositionName
         cls.CompositionChosen(L_D:VCID)
      IF NOT Window{PROP:AcceptAll}
        IF L_SD:CompositionName OR ?L_SD:CompositionName{PROP:Req}
          VCO:CompositionName = L_SD:CompositionName
          IF Access:VehicleComposition.TryFetch(VCO:Key_Name)
            IF SELF.Run(2,SelectRecord) = RequestCompleted
              L_SD:CompositionName = VCO:CompositionName
              L_D:VCID = VCO:VCID
            ELSE
              CLEAR(L_D:VCID)
              SELECT(?L_SD:CompositionName)
              CYCLE
            END
          ELSE
            L_D:VCID = VCO:VCID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:5
      ThisWindow.Update()
      VCO:CompositionName = L_SD:CompositionName
      IF SELF.Run(2,SelectRecord) = RequestCompleted
        L_SD:CompositionName = VCO:CompositionName
        L_D:VCID = VCO:VCID
      END
      ThisWindow.Reset(1)
         cls.CompositionChosen(L_D:VCID)
    OF ?L_SD:Horse_Registration
      Cur_TTID_# = L_D:TTID_FreightLiner
      IF NOT Window{PROP:AcceptAll}
        IF L_SD:Horse_Registration OR ?L_SD:Horse_Registration{PROP:Req}
          TRU:Registration = L_SD:Horse_Registration
          IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
            IF SELF.Run(3,SelectRecord) = RequestCompleted
              L_SD:Horse_Registration = TRU:Registration
              L_D:TTID_FreightLiner = TRU:TTID
            ELSE
              CLEAR(L_D:TTID_FreightLiner)
              SELECT(?L_SD:Horse_Registration)
              CYCLE
            END
          ELSE
            L_D:TTID_FreightLiner = TRU:TTID
          END
        END
      END
      ThisWindow.Reset()
         IF Cur_TTID_# <> L_D:TTID_FreightLiner            ! On change clear trailers
            CLEAR(L_D:TTID_SuperLink)
            CLEAR(L_D:TTID_Trailer)
            CLEAR(L_SD:Trailer1_Reg)
            CLEAR(L_SD:Trailer2_Reg)
            CLEAR(L_SD:Driver)
         .
         Cls.Check_Driver(L_D:TTID_FreightLiner)
    OF ?CallLookup:2
      ThisWindow.Update()
      Cur_TTID_# = L_D:TTID_FreightLiner
      TRU:Registration = L_SD:Horse_Registration
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        L_SD:Horse_Registration = TRU:Registration
        L_D:TTID_FreightLiner = TRU:TTID
      END
      ThisWindow.Reset(1)
         IF Cur_TTID_# <> L_D:TTID_FreightLiner            ! On change clear trailers
            CLEAR(L_D:TTID_SuperLink)
            CLEAR(L_D:TTID_Trailer)
            CLEAR(L_SD:Trailer1_Reg)
            CLEAR(L_SD:Trailer2_Reg)
            CLEAR(L_SD:Driver)
         .
         Cls.Check_Driver(L_D:TTID_FreightLiner)
    OF ?L_SD:Driver
      IF NOT Window{PROP:AcceptAll}
        IF L_SD:Driver OR ?L_SD:Driver{PROP:Req}
          DRI:FirstNameSurname = L_SD:Driver
          IF Access:Drivers.TryFetch(DRI:SKey_FirstNameSurname)
            IF SELF.Run(4,SelectRecord) = RequestCompleted
              L_SD:Driver = DRI:FirstNameSurname
              L_D:DRID = DRI:DRID
            ELSE
              CLEAR(L_D:DRID)
              SELECT(?L_SD:Driver)
              CYCLE
            END
          ELSE
            L_D:DRID = DRI:DRID
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:4
      ThisWindow.Update()
      DRI:FirstNameSurname = L_SD:Driver
      IF SELF.Run(4,SelectRecord) = RequestCompleted
        L_SD:Driver = DRI:FirstNameSurname
        L_D:DRID = DRI:DRID
      END
      ThisWindow.Reset(1)
    OF ?L_SD:Trailer1_Reg:2
      IF L_SD:Trailer1_Reg OR ?L_SD:Trailer1_Reg:2{PROP:Req}
        TRU:Registration = L_SD:Trailer1_Reg
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_SD:Trailer1_Reg = TRU:Registration
            L_D:TTID_Trailer = TRU:TTID
          ELSE
            CLEAR(L_D:TTID_Trailer)
            SELECT(?L_SD:Trailer1_Reg:2)
            CYCLE
          END
        ELSE
          L_D:TTID_Trailer = TRU:TTID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Trailer1
      ThisWindow.Update()
      TRU:Registration = L_SD:Trailer1_Reg
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_SD:Trailer1_Reg = TRU:Registration
        L_D:TTID_Trailer = TRU:TTID
      END
      ThisWindow.Reset(1)
    OF ?L_SD:Trailer2_Reg
      IF L_SD:Trailer2_Reg OR ?L_SD:Trailer2_Reg{PROP:Req}
        TRU:Registration = L_SD:Trailer2_Reg
        IF Access:TruckTrailer.TryFetch(TRU:Key_Registration)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            L_SD:Trailer2_Reg = TRU:Registration
            L_D:TTID_SuperLink = TRU:TTID
          ELSE
            CLEAR(L_D:TTID_SuperLink)
            SELECT(?L_SD:Trailer2_Reg)
            CYCLE
          END
        ELSE
          L_D:TTID_SuperLink = TRU:TTID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup_Trailer2
      ThisWindow.Update()
      TRU:Registration = L_SD:Trailer2_Reg
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        L_SD:Trailer2_Reg = TRU:Registration
        L_D:TTID_SuperLink = TRU:TTID
      END
      ThisWindow.Reset(1)
    OF ?L_SD:Journey
      IF L_SD:Journey OR ?L_SD:Journey{PROP:Req}
        JOU:Journey = L_SD:Journey
        IF Access:Journeys.TryFetch(JOU:Key_Journey)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            L_SD:Journey = JOU:Journey
            L_D:JID = JOU:JID
          ELSE
            CLEAR(L_D:JID)
            SELECT(?L_SD:Journey)
            CYCLE
          END
        ELSE
          L_D:JID = JOU:JID
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:3
      ThisWindow.Update()
      JOU:Journey = L_SD:Journey
      IF SELF.Run(7,SelectRecord) = RequestCompleted
        L_SD:Journey = JOU:Journey
        L_D:JID = JOU:JID
      END
      ThisWindow.Reset(1)
    OF ?BUTTON_Next
      ThisWindow.Update()
      Cls.Next()
    OF ?BUTTON_Back
      ThisWindow.Update()
      Cls.Back()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all window specific events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
         Cls.UpdTab(1)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

Cls.Next             PROCEDURE(SIGNED p_Field=0)
NextTab                 BYTE(1)        ! 1 is for continue, 0 is for stay where we are, 2 is for finish
Msgs                    STRING(255)

CODE   
   ! do stuff here for completion of tab then advance tab position
   ! 1 = Capacity,  2 = Transporter,  3 = Horse,  4 = Trailers, 5 = Journey
   ! p_Field can be the field that sent us here, as in the button   
   SELF.p_Field_Process(p_Field)   
   
   IF NextTab = 1                      ! User is going to the next tab, validate the tab & process anything for that
      CASE SELF.WizardTab
      OF 2                             ! Selected our Transporter
         TRA:TID = L_D:TID
         IF Access:Transporter.TryFetch(TRA:PKey_TID) = Level:Benign
            IF TRA:Status >= 2
               MESSAGE('The selected Transporter is not allowed.  Speak to Creditors.','Transporter Not Allowed', ICON:Hand)
               NextTab = 0
         .  .
      OF 3                             ! Selected our Horse, so check trailer info
         SELF.Check_Driver(L_D:TTID_FreightLiner)
         
         IF CLIP(L_SD:Horse_Registration) = ''
            CLEAR(L_D:TTID_FreightLiner)
            NextTab = 0
         ELSIF CLIP(L_SD:Driver) = ''
            CLEAR(L_D:DRID)
            NextTab = 0
         ELSE
            ! We need to know if a Horse has been left as it was before or re-selected (if gone back)
            ! This is checked on the Horse lookup controls
            ENABLE(?GROUP_Trailer2)    ! then we check how many trailers to allow
            IF SELF.Trailers = 0       ! no trailers
               CLEAR(L_SD:Trailer1_Reg)
               CLEAR(L_SD:Trailer2_Reg)
               CLEAR(L_D:TTID_Trailer)
               CLEAR(L_D:TTID_SuperLink)
               NextTab  = 2            ! Advance 2 tabes (skip trailers)
            ELSIF SELF.Trailers = 1    
               CLEAR(L_SD:Trailer2_Reg)
               CLEAR(L_D:TTID_SuperLink)
               DISABLE(?GROUP_Trailer2)
            .         
            
            ! Load the Trailers based on the horse, if the horse has a composition
            IF SELF.Trailers > 0 AND SELF.CompositionChosen = FALSE
               SELF.GetComposition(L_D:TTID_FreightLiner)
         .  .
      OF 4
         IF CLIP(L_SD:Trailer1_Reg) = ''
            CLEAR(L_D:TTID_Trailer)
         .
         IF CLIP(L_SD:Trailer2_Reg) = ''
            CLEAR(L_D:TTID_SuperLink)
         .            
            
         Msgs = 'TTID1: ' & L_D:TTID_Trailer & '  TTID2: ' & L_D:TTID_SuperLink
         IF SELF.Trailers = 1          ! We only want 1 trailer
            IF L_D:TTID_Trailer <= 0 AND L_D:TTID_SuperLink > 0   ! We have a SuperLink but no normal trailer, stop
               NextTab  = 0
         .  .
         IF SELF.Trailers = 2          ! We want 2 trailers
            IF L_D:TTID_Trailer = 0 OR L_D:TTID_SuperLink = 0     ! We have at least one missing
               NextTab  = 0
         .  .
      OF 5                             ! Selected our Journey, check it was selected
         IF L_D:JID = 0
            NextTab = 0                ! No advance then, need to select a Journey   
   .  .  .

   IF NextTab AND (SELF.WizardTab >= LOC:Cfg_LastTabNo)  ! We're on the last tab, and are moving fwd so go to Finish next
      NextTab = 100            
   .
   ?PROMPT_DBG{PROP:Text} = '[Next] SELF.WizardTab: ' & SELF.WizardTab & '  NextTab: ' & NextTab &  '   ' & Msgs
            
   SELF.SetTab(NextTab)
   RETURN

Cls.SetTab           PROCEDURE(LONG p_Tab)
l_NextTab               BYTE           ! 100 is finish
CODE
   IF p_Tab > 0 AND SELF.WizardTab = 100                    ! Advance on Finish tab, so finish then
      SELF.Finsihed()
   ELSE
      IF p_Tab = 100                                        ! Goto finish tab
         l_NextTab            = 100
         SELF.FromTabToFinish = SELF.WizardTab              ! The tab we were on before going to finish, for use with back
      ELSE
         l_NextTab            = SELF.WizardTab + p_Tab      ! Default move fwd/bkw
         IF p_Tab = -1           
            IF SELF.WizardTab = 100                         ! Going back from Finish
               l_NextTab      = SELF.FromTabToFinish        ! Where we went from when we went to finish earlier
         .  .
!         message('l_NextTab > LOC:Cfg_LastTabNo: ' & l_NextTab & ' > ' & LOC:Cfg_LastTabNo)
         IF l_NextTab > LOC:Cfg_LastTabNo
            l_NextTab = LOC:Cfg_LastTabNo
         ELSIF l_NextTab < 1
            l_NextTab = 1
         .
      .
      SELF.UpdTab(l_NextTab)
   .
   RETURN   
 
! ===================================================================   

Cls.Back             PROCEDURE()
CODE
   SELF.SetTab(-1) 
   RETURN

! -----------------------------------------------------------------------
Cls.UpdTab           PROCEDURE(LONG p_Tab)
CODE
   ! Set buttons, no next / back on tab 1
   ENABLE(?BUTTON_Next)
   ENABLE(?BUTTON_Back)
   IF p_Tab = 1
      HIDE(?BUTTON_Back)
      HIDE(?BUTTON_Next)
   ELSE
      UNHIDE(?BUTTON_Back)
      UNHIDE(?BUTTON_Next)
   .   
   
   ! Check when to add Finish button & Run validation etc.
   IF p_Tab = 100
      ?BUTTON_Next{PROP:Text}    = 'Finish'
      SELF.Check_Finish_Ready()
   ELSE
      ?BUTTON_Next{PROP:Text}    = '&Next'
   .
   
   EXECUTE p_Tab
      SELECT(?TAB_Capacity)
      SELECT(?L_SD:TransporterName)
      SELECT(?L_SD:CompositionName)
      SELECT(?L_SD:Trailer1_Reg:2)
      SELECT(?L_SD:Journey)
   ELSE
      IF p_Tab = 100
         SELECT(?TAB_Finish)
      ELSE
         ! ?
   .  . 
   SELF.WizardTab = p_Tab
   RETURN
   
Cls.p_Field_Process PROCEDURE(SIGNED p_Field)   
CODE
   CASE p_Field
   OF ?BUTTON_Next OROF 0
      ! normal stuff
   ELSE     ! IF SELF.WizardTab = 1       We have special buttons for this one
      CASE p_Field
      OF ?BUTTON_Link33                ! 2 trailers
         L_SD:Capacity  = 33
         SELF.Trailers  = 2
         SELF.Cost      = L_SD:Link_cost
         ?PROMPT_Selected{PROP:Ypos}   = ?L_SD:Link_cost{PROP:Ypos}
      OF ?BUTTON_Tri                   ! 1 trailer
         L_SD:Capacity  = 22
         SELF.Trailers  = 1
         SELF.Cost      = L_SD:Tri_cost
         ?PROMPT_Selected{PROP:Ypos}   = ?L_SD:Tri_cost{PROP:Ypos}
      OF ?BUTTON_15Ton                 ! 0 trailers
         L_SD:Capacity  = 15
         SELF.Trailers  = 0
         SELF.Cost      = L_SD:Ton15_cost
         ?PROMPT_Selected{PROP:Ypos}   = ?L_SD:Ton15_cost{PROP:Ypos}
      OF ?BUTTON_8Ton                  ! 0 trailers
         L_SD:Capacity  = 8
         SELF.Trailers  = 0
         SELF.Cost      = L_SD:Ton8_cost
         ?PROMPT_Selected{PROP:Ypos}   = ?L_SD:Ton15_cost{PROP:Ypos}
      OF ?BUTTON_5Ton                  ! 0 trailers 
         L_SD:Capacity  = 5  
         SELF.Trailers  = 0
         SELF.Cost      = L_SD:Ton5_cost
         ?PROMPT_Selected{PROP:Ypos}   = ?L_SD:Ton15_cost{PROP:Ypos}
      OF ?BUTTON_2Ton                  ! 0 trailers
         L_SD:Capacity  = 2
         SELF.Trailers  = 0
         SELF.Cost      = L_SD:Ton2_cost
         ?PROMPT_Selected{PROP:Ypos}   = ?L_SD:Ton2_cost{PROP:Ypos}
      .
      UNHIDE(?PROMPT_Selected)
   .  
   RETURN

Cls.Construct        PROCEDURE()
CODE
   SELF.WizardTab    = 1
   SELF.Trailers     = 2
   L_SD:Link_cost    = GETINI('Upd_Manifest_Wizard', 'Link_Cost', 5000, GLO:Global_INI)
   L_SD:Tri_cost     = GETINI('Upd_Manifest_Wizard', 'Tri_Cost', 4000, GLO:Global_INI)
   L_SD:Ton15_cost   = GETINI('Upd_Manifest_Wizard', 'Ton15_Cost', 3500, GLO:Global_INI)
   L_SD:Ton8_cost    = GETINI('Upd_Manifest_Wizard', 'Ton8_Cost', 3300, GLO:Global_INI)
   L_SD:Ton5_cost    = GETINI('Upd_Manifest_Wizard', 'Ton5_Cost', 3200, GLO:Global_INI)
   L_SD:Ton2_cost    = GETINI('Upd_Manifest_Wizard', 'Ton2_Cost', 3000, GLO:Global_INI)
   RETURN
   
Cls.Destruct         PROCEDURE()
CODE
   RETURN

   
!     Using the TTID get the latest (highest ID) composition and populate from it   
Cls.GetComposition   PROCEDURE(ULONG p_TTID)
p_VCID_High    ULONG
CODE
   CLEAR(VCO:Record, +1)
   VCO:TTID0   = p_TTID
   SET(VCO:FKey_TID0, VCO:FKey_TID0)
   LOOP
      IF Access:VehicleComposition.TryNext() ~= LEVEL:Benign
         BREAK
      .
      db.debugout('start - p_ttid: ' & p_TTID)
      IF VCO:TTID0 ~= p_TTID
         BREAK
      .
      
      db.debugout('VCO:VCID: ' & VCO:VCID)
      IF p_VCID_High < VCO:VCID
         p_VCID_High          = VCO:VCID
         L_D:TTID_Trailer     = VCO:TTID1
         L_D:TTID_SuperLink   = VCO:TTID2         
      .      
   .
   IF p_VCID_High = 0
      ! none found
   ELSE
      Cls.Set_Trucks()
   .
   DISPLAY()
   RETURN
   
   
Cls.Check_Vehicle_Composition    PROCEDURE
CODE
   IF L_D:VCID = 0
      ! We need to create one
      ! (p_TID, p_CompositionName, p_FreightLinerTTID, p_TrailerTTID, p_SuperLinkTTID, p_Capicity)
      L_D:VCID    = Add_Vehicle_Composition(L_D:TID, L_SD:NewCompositionName, L_D:TTID_FreightLiner, L_D:TTID_Trailer, L_D:TTID_SuperLink, SELF.ActualCapacity)

      IF L_D:VCID = 0
         MESSAGE('Could not create a new Vehicle Composition.||Check that the New name is unique.','Vehicle Composition',ICON:Hand)
         SELECT(?L_SD:NewCompositionName)
  .  .  
  RETURN
   

Cls.Check_Driver                 PROCEDURE(ULONG p_TID)
CODE
   ! Also set Driver now
   A_TRU:TTID     = p_TID
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
      DRI:DRID    = A_TRU:DRID
      IF Access:Drivers.TryFetch(DRI:PKey_DRID) = Level:Benign
         L_D:DRID       = A_TRU:DRID
         L_SD:Driver    = CLIP(DRI:FirstName) & ' ' & DRI:Surname
         DISPLAY(?L_SD:Driver)
   .  . 
   RETURN

   
Cls.Set_Trucks                   PROCEDURE()
CODE
   IF L_D:TTID_Trailer = 0          ! If no trailer then set trailer to superlink 
      L_D:TTID_Trailer  = L_D:TTID_SuperLink
      CLEAR(L_D:TTID_SuperLink)
   .
   
   !L_D:VCID             = p_VCID_High
   A_TRU:TTID        = L_D:TTID_Trailer
   IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
      L_SD:Trailer1_Reg = A_TRU:Registration
   .   

   IF SELF.Trailers < 2 
      CLEAR(L_D:TTID_SuperLink)
   ELSE
      A_TRU:TTID           = L_D:TTID_SuperLink
      IF Access:TruckTrailerAlias.TryFetch(A_TRU:PKey_TTID) = Level:Benign
         L_SD:Trailer2_Reg = A_TRU:Registration
      .
   .
   DISPLAY()
   RETURN
Cls.Check_Finish_Ready           PROCEDURE
l_ListTTIDs                         STRING(200)
l_Res                               STRING(100)

l_Capacity_Res                      STRING(500)
l_VCO_Res                           STRING(500)
l_TID_Res                           STRING(500)

CODE
   ! Check Capactiy selected is matched by vehicles selected?
   Add_to_List(L_D:TTID_FreightLiner, l_ListTTIDs, ',')
   Add_to_List(L_D:TTID_Trailer, l_ListTTIDs, ',')
   Add_to_List(L_D:TTID_SuperLink, l_ListTTIDs, ',')
   ! Get total capacity of selected vehicles
   _SQL.PropSQL('SELECT SUM(Capacity) FROM TruckTrailer WHERE TTID IN(' & CLIP(l_ListTTIDs) & ')')
   _SQL.Next_Q()
   SELF.ActualCapacity     = _SQL.Data_G.F1
   
   Add_to_List('The selected capacity is ' & (L_SD:Capacity * 1000), l_Capacity_Res, EQU:CRLF)
   Add_to_List('The chosen vehicle (and any trailers) capacity is ' & _SQL.Data_G.F1, l_Capacity_Res, EQU:CRLF)
   IF _SQL.Data_G.F1 < (L_SD:Capacity * 1000)
      Add_to_List('**** This is LESS than the selected capacity ****', l_Capacity_Res, EQU:CRLF)
   .
   
         
   ! Check if we have a current VCO record that matches selected TTIDs, if so set this
   _SQL.Log = 10
   IF _SQL.PropSQL('SELECT VCID FROM VehicleComposition WHERE TTID0 = ' & L_D:TTID_FreightLiner & ' AND (TTID1 = ' & L_D:TTID_Trailer & | 
      ' OR TTID1 = ' & L_D:TTID_SuperLink & ') AND (TTID2 = ' & L_D:TTID_SuperLink & ' OR TTID2 = ' & L_D:TTID_Trailer & ')  AND Archived = 0') > 0
!      db.Debugout(' -------- _SQL.Data_G.F1: ' & _SQL.Data_G.F1)
      _SQL.Next_Q()      
      L_D:VCID       = _SQL.Data_G.F1
      HIDE(?L_SD:NewCompositionName)
      HIDE(?L_SD:NewCompositionName:Prompt)
   ELSE
      ! New VCO needed, and ask user to proceed.
      l_VCO_Res      = 'There is no Vehicle Composition that matches the chosen Horse & Trailers.<13,10>A new Vehicle Composition will be added if you click finish.'
      L_D:VCID       = 0
      
      i# = 0
      LOOP 5 TIMES
         i# += 1
         VCO:CompositionName = 'm' & i# & '. ' & CLIP(L_SD:TransporterName) & ' - ' & L_SD:Horse_Registration
         IF Access:VehicleComposition.TryFetch(VCO:Key_Name) <> Level:Benign
            L_SD:NewCompositionName    = VCO:CompositionName
            BREAK
      .  .
      
      UNHIDE(?L_SD:NewCompositionName)
      UNHIDE(?L_SD:NewCompositionName:Prompt)
   .   
   
   ! Check if not default transporter used, if not then zero the value
   IF L_DGG:Default_TID <> L_D:TID
      Add_to_List('The selected Transporter is not the Default one, Value of Manifest will need to be set on the Manifest.', l_TID_Res, EQU:CRLF)
   .

   IF L_D:Broking = TRUE
      Add_to_List('This is a Broking transporter.', l_TID_Res, EQU:CRLF)
   ELSE
      Add_to_List('This is a non-Broking transporter.', l_TID_Res, EQU:CRLF)
   .
      

   ?TEXT_Fin{PROP:Text} = CLIP(l_Capacity_Res) & EQU:CRLF & EQU:CRLF & CLIP(l_VCO_Res) & EQU:CRLF & EQU:CRLF & CLIP(l_TID_Res)
   DISPLAY()
   RETURN
Cls.Finsihed                              PROCEDURE()
CODE
   ! Save costs
   PUTINI('Upd_Manifest_Wizard', 'Link_Cost', L_SD:Link_cost    , GLO:Global_INI)
   PUTINI('Upd_Manifest_Wizard', 'Tri_Cost', L_SD:Tri_cost     , GLO:Global_INI)
   PUTINI('Upd_Manifest_Wizard', 'Ton15_Cost', L_SD:Ton15_cost   , GLO:Global_INI)
   PUTINI('Upd_Manifest_Wizard', 'Ton8_Cost', L_SD:Ton8_cost   , GLO:Global_INI)
   PUTINI('Upd_Manifest_Wizard', 'Ton5_Cost', L_SD:Ton5_cost   , GLO:Global_INI)
   PUTINI('Upd_Manifest_Wizard', 'Ton2_Cost', L_SD:Ton2_cost    , GLO:Global_INI)

   ! Do whatever we need to setup the Manifest, then... open the manifest for capture
   
   ! Check Vehicle Composition, if not available then add a new one
   SELF.Check_Vehicle_Composition()
   IF L_D:VCID = 0
      ! We dont have a valid Vehichle Composition yet, so can't proceed, user already informed
   ELSE
      CLEAR(MAN:Record)
      IF Access:Manifest.PrimeAutoInc() ~= Level:Benign
         MESSAGE('Problem with adding a Manifest, please contact support.||' & ERROR())
      ELSE
         ! get transporter - if broking
         IF L_D:Broking = TRUE
            TRA:TID = L_D:TID
            IF Access:Transporter.TryFetch(TRA:PKey_TID) = Level:Benign
               IF TRA:ChargesVAT = FALSE                     ! Check VAT
                  MAN:VATRate  = 0.0
               ELSIF MAN:VATRate = 0.0
                  MAN:VATRate  = Get_Setup_Info(4)
               .
            .
         .
         
         MAN:CreatedDate         = TODAY()
         MAN:CreatedTime         = CLOCK()         
         MAN:BID                 = GLO:BranchID
         MAN:TID                 = L_D:TID
         MAN:Broking             = L_D:Broking
         MAN:VCID                = L_D:VCID
         
         IF L_DGG:Default_TID <> L_D:TID
            MAN:Cost             = 0.0
         ELSE
            MAN:Cost             = SELF.Cost
         .         
         MAN:JID                 = L_D:JID
         MAN:DRID                = L_D:DRID
         !MAN:Rate
         !MAN:VATRate
         !MAN:Broking
         !MAN:DRID
         MAN:TTID_FreightLiner   = L_D:TTID_FreightLiner
         MAN:TTID_Trailer        = L_D:TTID_Trailer
         MAN:TTID_SuperLink      = L_D:TTID_SuperLink
         
         IF Access:Manifest.TryInsert() ~= Level:Benign
            MESSAGE('Problem with adding a Manifest, please contact support.||' & ERROR())
         ELSE
            Window{PROP:Hide}    = TRUE
            GlobalRequest        = ChangeRecord
            Update_Manifest(1)                        ! 1 = go to Loading DIs right away
            POST(EVENT:CloseWindow)               
   .  .  .
   RETURN   
Cls.CompositionChosen      PROCEDURE(p:VCID)
CODE
   ! Clear these on selection
   SELF.CompositionChosen = TRUE
   DISABLE(?GROUP_Horse)
   
   CLEAR(L_D:TTID_SuperLink)
   CLEAR(L_D:TTID_Trailer)
   CLEAR(L_SD:Trailer1_Reg)
   CLEAR(L_SD:Trailer2_Reg)
   CLEAR(L_SD:Driver)


   VCO:VCID    = p:VCID
   IF Access:VehicleComposition.TryFetch(VCO:PKey_VCID) = Level:Benign
      L_D:VCID                = VCO:VCID

      L_D:TTID_FreightLiner   = VCO:TTID0
      L_D:TTID_SuperLink      = VCO:TTID1
      L_D:TTID_Trailer        = VCO:TTID2

      
      db.debugout('[Cls.CompositionChosen]  VCO:TTID0: ' & VCO:TTID0 & '   VCO:TTID1: ' & VCO:TTID1 & '   VCO:TTID2: ' & VCO:TTID2)
      
      
      TRU:TTID                = VCO:TTID0
      IF Access:TruckTrailer.TryFetch(TRU:PKey_TTID) = Level:Benign
         L_SD:Horse_Registration = TRU:Registration         
         
         DRI:DRID                = TRU:DRID
         IF Access:Drivers.TryFetch(DRI:PKey_DRID) = Level:Benign
            L_SD:Driver       = CLIP(DRI:FirstName) & ' ' & DRI:Surname
            L_D:DRID          = DRI:DRID
         .
      .
      Cls.Set_Trucks()
   .

   DISPLAY()         
   RETURN
!!! <summary>
!!! Generated from procedure template - Source
!!! </summary>
Truck_and_Trailer_Capacity PROCEDURE                       ! Declare Procedure

  CODE
